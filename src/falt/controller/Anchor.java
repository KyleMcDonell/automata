package falt.controller;

import java.awt.geom.Point2D;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 * The anchor is used to define the transition arcs using its position. 
 * It includes drag-and-drop control functionality, so the user can 
 * reposition it with the mouse.  This action is an ActionPattern
 * 
 * @author ncameron
 * @author kmcdonell
 */
public class Anchor {
	
	/** The visual component of the anchor. */
	private AnchorCircle c;
	/** The undo manager for managing the actions on the anchor. */
	private UndoManager undoManager;
	
	/**
	 * Initializes a new anchor with the given color, x and y position, and
	 * undo manager for managing actions.
	 * 
	 * @param color		the Color of the anchor
	 * @param x			the x position of the anchor
	 * @param y			the y position of the anchor
	 * @param undoManager	the UndoManager to handle movement actions
	 */
	public Anchor(Color color, double x, double y, UndoManager undoManager){
		c = new AnchorCircle(x,y,5);
		c.setFill(color);
	    c.centerXProperty().set(x);
	    c.centerYProperty().set(y);
	    this.undoManager = undoManager;
	    enableInteraction();
	}
	
	/**
	 * Returns the visual component of the anchor.
	 * @return	the AnchorCircle for this Anchor
	 */
	public AnchorCircle getVisual(){
		return c;
	}
	
	/**
	 * Makes the node movable by dragging with the mouse.
	 */
	private void enableInteraction() {
		
		// Enable drag
		final Point2D.Double dragOffset = new Point2D.Double();
		final Point2D.Double dragStart = new Point2D.Double();
		final BooleanProperty moved = new SimpleBooleanProperty(false);
		
		// Changes the cursor to a closed hand while dragging
		c.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				// record a Offset distance for the drag and drop operation.
				dragOffset.x = c.getCenterX() - mouseEvent.getX();
				dragOffset.y = c.getCenterY() - mouseEvent.getY();
				dragStart.x = c.getCenterX();
				dragStart.y = c.getCenterY();
				c.getScene().setCursor(Cursor.CLOSED_HAND);
				mouseEvent.consume();
			}
		});
		
		// Updates the AnchorCircle position when dragged
		c.setOnMouseDragged(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				moved.set(true);
				double newX = mouseEvent.getX() + dragOffset.x;
				if (newX > 0 && newX < c.getScene().getWidth()) {
					c.setCenterX(newX);
				}
				double newY = mouseEvent.getY() + dragOffset.y;
				if (newY > 0 && newY < c.getScene().getHeight()) {
					c.setCenterY(newY);
				}
				mouseEvent.consume();
			}
		});
		
		// Changes the cursor to an open hand after dragging
		c.setOnMouseReleased(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				c.getScene().setCursor(Cursor.OPEN_HAND);
				if (moved.get()){
					double dx = c.getCenterX()-dragStart.x;
					double dy = c.getCenterY()-dragStart.y;
					if (Math.abs(dx) > 0.1 || Math.abs(dy) > 0.1)
						undoManager.addAction(new CircleMovementAction(c,dx,dy));
					moved.set(false);
				}
				mouseEvent.consume();
			}
		});
		
		// Changes the mouse to an open hand for dragging
		c.setOnMouseEntered(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				if (!mouseEvent.isPrimaryButtonDown()) {
					c.getScene().setCursor(Cursor.OPEN_HAND);
				}
				mouseEvent.consume();
			}
		});
		
		// Changes the mouse to the default cursor
		c.setOnMouseExited(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				if (!mouseEvent.isPrimaryButtonDown()) {
					c.getScene().setCursor(Cursor.DEFAULT);
				}
				mouseEvent.consume();
			}
		});
		
		// Hides the anchor if it loses focus
		c.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldValue, Boolean newValue) {
				c.setVisible(!newValue);
			}
		});
		
	}
	
	/**
	 * Provides an extension of the circle class for the visual component of the 
	 * anchor.  This allows anchor points to be differentiated from circles.
	 * 
	 * @author ncameron
	 * @author kmcdonell
	 */
	public static class AnchorCircle extends Circle {

		public AnchorCircle() {
			super();
		}

		public AnchorCircle(double x, double y, double r) {
			super(x, y, r);
		}
	}
	
}
	
