package falt.controller;

import java.awt.geom.Point2D;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import falt.model.FiniteAutomata;
import falt.model.State;
import falt.model.Transition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

/**
 * A controller for the visual components of the automata in the view, responsible 
 * for controlling the automata, saving, and testing.
 * 
 * @author ncameron
 * @author kmcdonell
 */
public class AutomataControl implements Serializable {

	private static final long serialVersionUID = -2979926683352539488L;
	
	/** Holds the finite automata instance. */
	private transient FiniteAutomata automata;	
	/** Holds the view pane, where the automata's visual elements are displayed. */
	private transient Pane pane;
	/** The editor handles most actions taken on the pane. */
	private transient EditorController editor;
	/** Keeps track of actions taken to allow undo/redo */
	private transient UndoManager undoManager;
	/** The file this automata is saved in, or null if it is not saved */
	private transient File file;
	
	/** A mapping of the states in the finite automata to their controllers. */
	private transient HashMap<State,StateControl> stateMap = new HashMap<>();
	/** A mapping of the transitions in the FA to their controllers. */
	private transient HashMap<Transition,TransitionControl> transitionMap = new HashMap<>();
	
	/** Allows transitions to be created via "drag and drop". */
	private transient StateControl dragSource;
	
	
	/** Constructs a new AutomataControl */
	public AutomataControl(){
		automata = new FiniteAutomata();
		pane = new Pane();
		undoManager = new UndoManager();
		enableInteraction();
	}
	
	/** 
	 * Constructs a new AutomataControl with a given editor, based on an 
	 * existing FiniteAutomata.  States of the FiniteAutomata are added
	 * in a checkerboard formation.
	 */
	public AutomataControl(FiniteAutomata fa){
		automata = fa;
		pane = new Pane();
		undoManager = new UndoManager();
		
		// Adds the states in a checker board
		int offsetX = 90;
		int offsetY = 90;
		int row = 1;
		for (State st: fa.getStates()){
			addState(st, offsetX, offsetY);
			offsetX += 70;
			offsetY = offsetY + row*70;
			row = -row;
			if (offsetX > 770){
				offsetX = 90;
				offsetY += 140;
			}
		} 
		
		
		// Add the transitions
		for (Transition t : fa.getTransitions()){
			addTransition(stateMap.get(t.getSource()),
					stateMap.get(t.getTarget()), t);
		}
		
		undoManager.clear();
		enableInteraction();
	}
	
	/**
	 * Returns the start state controller.
	 * @return	the StateControl for the start state of the FiniteAutomata 
	 * 			controlled by this AutomataControl instance
	 */
	public StateControl getStartState(){ 
		return (stateMap != null) ? stateMap.get(automata.getStart()) : null; }
	
	/**
	 * Sets the start state controller to the given state controller.
	 * 
	 * @param stateControl	the StateControl for the new start state
	 */
	public void setStartState(StateControl stateControl){
		StateControl oldStart = stateMap.get(automata.getStart());
		if (stateControl != null){
			automata.setStart(stateControl.getState());
			stateControl.updateVisuals();
		} else {
			automata.setStart(null);
		}
		if (oldStart != null){
			oldStart.updateVisuals();
		}
	}
	
	/**
	 * Returns the interaction state of the editor.
	 * @return		the current InteractState of the editor
	 */
	public InteractState getInteractState(){ return editor.getInteractState(); }
	
	/**
	 * Returns the view pane.
	 * @return the Pane for the view
	 */
	public Pane getViewPane(){ return pane; }
	
	/**
	 * Returns the finite automata that this controller is responsible for.
	 * @return	the FiniteAutomata for this controller
	 */
	public FiniteAutomata getAutomata(){ return automata; }
	
	/**
	 * Returns the undo manager for this controller.
	 * @return	the UndoManager for this controller
	 */
	public UndoManager getUndoManager(){ return undoManager; }
	
	/**
	 * Returns the state map for this controller.
	 * @return	a HashMap of States to StateControls for this controller
	 */
	public HashMap<State,StateControl> getStateMap(){ return stateMap; }
	
	/**
	 * Returns the transition map for this controller.
	 * @return	a HashMap of Transitions to TransitionControls for this controller
	 */
	public HashMap<Transition,TransitionControl> getTransitionMap(){ return transitionMap; }
	
	/**
	 * Returns the file that this controller is associated with.
	 * @return	the File for this controller
	 */
	public File getFile(){ return file; }
	
	/**
	 * Sets the file that this controller is associated with.
	 * @param file		the new File
	 */
	public void setFile(File file){ this.file = file; }
	
	/**
	 * Sets the editor for this controller to the given editor.
	 * @param editor	the new EditorController
	 */
	public void setEditor(EditorController editor){ 
		this.editor = editor; 
		enablePaneExpansion();
	}
	
	/**
	 * Resets the position of all transition anchors to the default.
	 */
	public void resetAllAnchors(){
		// Create a mapping of old and new positions
		Map<TransitionControl,Point2D.Double> oldPositions = 
				new HashMap<>(transitionMap.size());
		Map<TransitionControl,Point2D.Double> newPositions = 
				new HashMap<>(transitionMap.size());
		
		for (TransitionControl t : transitionMap.values()){
			oldPositions.put(t,t.getAnchorPosition());
			t.resetAnchorPosition();
			newPositions.put(t,t.getAnchorPosition());
		}
		
		undoManager.addAction(
				new VisualRepositionAction(null,oldPositions,null,newPositions));
	}
	
	/**
	 * Make all anchors visible.
	 */
	public void showAllAnchors(){
		for (TransitionControl t : transitionMap.values())
			t.showAnchor(true);
	}
	
	/**
	 * Make all anchors invisible.
	 */
	public void hideAllAnchors(){
		for (TransitionControl t : transitionMap.values())
			t.showAnchor(false);
	}
	
	/**
	 * Refreshes the visibility and position of the visual elements of the automata.
	 */
	public void updateVisuals(){
		for (StateControl s : stateMap.values())
			s.updateVisuals();
	}
	
	/**
	 * Returns the closest position to the origin for this automata controller.
	 * @return		the closest position to the origin.
	 */
	public Point2D.Double findMinimumBound(){
		Point2D.Double minPos = new Point2D.Double(Double.MAX_VALUE,Double.MAX_VALUE);
		for (StateControl s : stateMap.values()){
			if (s.getX() < minPos.x)
				minPos.x = s.getX();
			if (s.getY() < minPos.y)
				minPos.y = s.getY();	
		}
		for (TransitionControl t : transitionMap.values()){
			if (t.getAnchorX() < minPos.x)
				minPos.x = t.getAnchorX();
			if (t.getAnchorY() < minPos.y)
				minPos.y = t.getAnchorY();	
		}
		return minPos;
		
	}
	
	/**
	 * Reposition all visuals to be in the top left corner.
	 */
	public void moveToTopLeft(){
		Point2D.Double minPos = findMinimumBound();
		
		// If the visuals are already in the position they would be 
		// moved, return
		if (minPos.x == 100 && minPos.y == 100)
			return;
		
		// Otherwise move the visuals and create a mapping
		// for their old and new positions
		else {
			Map<StateControl,Point2D.Double> oldSPos = new HashMap<>();
			Map<TransitionControl,Point2D.Double> oldTPos = new HashMap<>();
			Map<StateControl,Point2D.Double> newSPos = new HashMap<>();
			Map<TransitionControl,Point2D.Double> newTPos = new HashMap<>();
			
			// Move the states and transitions so they are aligned with the axis at 100,100
			for (StateControl s : stateMap.values()){
				oldSPos.put(s,s.getStatePosition());
				s.setX(s.getX() - minPos.x + 100);
				s.setY(s.getY() - minPos.y + 75);
				newSPos.put(s,s.getStatePosition());
			}
			for (TransitionControl t : transitionMap.values()){
				oldTPos.put(t,t.getAnchorPosition());
				t.setAnchorX(t.getAnchorX() - minPos.x + 100);
				t.setAnchorY(t.getAnchorY() - minPos.y + 75);
				newTPos.put(t,t.getAnchorPosition());
			}
			
			undoManager.addAction(new VisualRepositionAction(oldSPos,oldTPos,newSPos,newTPos));
		}
		
	}
	
	/**
	 * Sets the source of a drag action to the given state controller.
	 * @param dragSource	the StateControl source of a drag action
	 */
	public void setDragSource(StateControl dragSource){
		this.dragSource = dragSource;
	}
	
	/**
	 * When a user is done dragging, add a transition between the two states 
	 * they dragged to and from, if possible.
	 * @param dragTarget	the target of the dragging
	 */
	public void dragEnded(StateControl dragTarget){
		if (automata.getTransition(dragSource.getState(), dragTarget.getState()) == null)
			addTransition(dragSource, dragTarget);
		dragSource = null;
	}
	
	/**
	 * Undoes the last action by the user.
	 * @return	true if possible, else false
	 */
	public boolean undoLastAction(){
		if (undoManager.getCanUndo()){
			undoManager.undo();
			return true;
		} else
			return false;
	}
	
	/**
	 * Redoes the last action by the user.
	 * @return	true if possible, else false
	 */
	public boolean redoLastAction(){
		if (undoManager.getCanRedo()){
			undoManager.redo();
			return true;
		} else
			return false;
	}
	
	/**
	 * Orders the visual elements to match the ordering presented in the 
	 * VisualComparator class.
	 */
	private void sortVisuals(){
		ObservableList<Node> workingCollection = 
				FXCollections.observableArrayList(pane.getChildren());
		Collections.sort(workingCollection, VisualComparator.getInstance());
		pane.getChildren().setAll(workingCollection);
	}
	
	/**
	 * Simulates a step on the testing of a string in the automata.
	 * @param c		the character to transition on
	 * @param prevStates	a set of previously active states
	 * @return		a set of states active after transitioning
	 */
	public Set<State> step(Character c, Set<State> prevStates){
		return automata.step(c, prevStates);
	}
	
	/**
	 * Given a set of states in the FA, highlights the corresponding visual
	 * elements in the editor.
	 * 
	 * @param states	the set of states to highlight
	 */
	public void colorStates(Set<State> states){
		for (State s : automata.getStates()){
			if (states != null && states.contains(s))
				stateMap.get(s).highlight(true);
			else
				stateMap.get(s).highlight(false);
		}
	}
	
	/**
	 * Adds a new state controller at (x,y).
	 */
	public void addState(double x, double y){
		State state = new State();
		addState(state,x,y);
	}
	
	/**
	 * Adds a new state controller from an existing state at (x,y).
	 */
	public void addState(State state, double x, double y){
		StateControl stateControl = new StateControl(x,y,state,this);
		undoManager.addAction(new AddStateAction(stateControl));
		addStateWithoutAction(stateControl);
	}
	
	/**
	 * Adds a new state controller without communicating with the undo manager.
	 * @param stateControl		the state to add
	 */
	private void addStateWithoutAction(StateControl stateControl){
		State state = stateControl.getState();
		stateMap.put(state, stateControl);
		automata.addState(state);
		pane.getChildren().addAll(stateControl.getVisuals());
		sortVisuals();
		
	}
	
	/**
	 * Removes a state controller from the automata controller, and removes all
	 * transitions to and from it.
	 * @param stateControl		the state controller to remove
	 */
	public void removeState(StateControl stateControl){
		
		State state = stateControl.getState();
		
		// List of transitions removed
		List<TransitionControl> removedTs = new ArrayList<>();
		
		// Remove all visual transitions to and from the state
		for (Transition t : state.getTransitions()){
			TransitionControl tControl = transitionMap.get(t);
			removedTs.add(tControl);
			pane.getChildren().removeAll(tControl.getVisuals());
			transitionMap.remove(t);
		}
		for (Transition t: automata.getTransitionsTo(state)){
			TransitionControl tControl = transitionMap.get(t);
			// Note a self loop will try to be removed twice!
			if (tControl != null) {
				removedTs.add(tControl);
				pane.getChildren().removeAll(tControl.getVisuals());
				transitionMap.remove(t);
			}
		}
		
		undoManager.addAction(new RemoveStateAction(stateControl,removedTs));
		// Note this removes transitions in the automata
		removeStateWithoutAction(stateControl);
	}
	
	
	/**
	 * Removes a state controller from the automata controller, assuming no visual
	 * transitions exist to or from the state, without communicating with the undo
	 * Manager
	 * @param stateControl		the state controller to remove
	 */
	private void removeStateWithoutAction(StateControl stateControl){
		State state = stateControl.getState();
		stateMap.remove(state);
		// Note this removes transitions in the automata
		automata.removeState(state);
		pane.getChildren().removeAll(stateControl.getVisuals());
	}
	
	/**
	 * Adds a transition controller between two state controllers.
	 * @param source	the source of the transition
	 * @param target	the target of the transition
	 */
	public void addTransition(StateControl source, StateControl target){
		Transition t = new Transition(source.getState(), target.getState());
		addTransition(source,target,t);
	}
	
	/**
	 * Adds a transition controller based on an existing transition between two 
	 * state controllers.
	 * 
	 * @param source	the source of the transition
	 * @param target	the target of the transition
	 */
	public void addTransition(StateControl source, StateControl target, Transition t){
		TransitionControl tControl = new TransitionControl(t,source,target,this);
		undoManager.addAction(new AddTransitionAction(tControl));
		addTransitionWithoutAction(tControl);
	}
	

	/**
	 * Adds a transition controller between two state controllers, without notifying 
	 * the undo manager.
	 * 
	 * @param tControl		the added transition controller
	 */
	private void addTransitionWithoutAction(TransitionControl tControl){
		Transition t = tControl.getTransition();
		transitionMap.put(t, tControl);
		automata.addTransition(t);
		pane.getChildren().addAll(tControl.getVisuals());
		sortVisuals();
	}
	
	/**
	 * Removes a transition controller from the automata controller.
	 * @param tControl		the transition controller to remove
	 */
	public void removeTransition(TransitionControl tControl){
		undoManager.addAction(new RemoveTransitionAction(tControl));
		removeTransitionWithoutAction(tControl);
		
		
	}

	/**
	 * Removes a transition controller from the automata controller without
	 * notifying the undo manager.
	 * @param tControl		the transition controller to remove
	 */
	private void removeTransitionWithoutAction(TransitionControl tControl){
		Transition t = tControl.getTransition();
		automata.removeTransition(t);
		transitionMap.remove(t);
		pane.getChildren().removeAll(tControl.getVisuals());
	}
	
	/**
	 * Initializes interactive methods and listeners.
	 */
	private void enableInteraction() {
		
		// Create a context menu for the pane
		ContextMenu rightClickMenu = new ContextMenu();
		// Create an item to reset all anchors to their default position
		MenuItem resetAnchorsMenuItem = new MenuItem("Reset Anchor Positions");
		resetAnchorsMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e){
				if (transitionMap.size() > 0)
					resetAllAnchors();
			}
		});
		// Create an item to show all transition anchors
		MenuItem showAnchorsMenuItem = new MenuItem("Show All Anchors");
		showAnchorsMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e){
				if (transitionMap.size() > 0)
					showAllAnchors();
			}
		});
		// Create an item to hide all transition anchors
		MenuItem hideAnchorsMenuItem = new MenuItem("Hide All Anchors");
		hideAnchorsMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e){
				if (transitionMap.size() > 0)
					hideAllAnchors();
			}
		});
		// Create an item to move the entire FA to the top-left
		MenuItem positionInTopLeftMenuItem = new MenuItem("Position in Top Left");
		positionInTopLeftMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e){
				if (stateMap.size() > 0)
					moveToTopLeft();
			}
		});
		// Add the above items to the context menu
		rightClickMenu.getItems().add(resetAnchorsMenuItem);
		rightClickMenu.getItems().add(showAnchorsMenuItem);
		rightClickMenu.getItems().add(hideAnchorsMenuItem);
		rightClickMenu.getItems().add(positionInTopLeftMenuItem);
		
		
		// When we are clicked do this.  The event is not consumed so that
		// child nodes can catch them
		pane.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override public void handle(MouseEvent mouseEvent) {
				// If in create mode and its a primary click, create a new state
				if (getInteractState() == InteractState.CREATE 
						&& mouseEvent.isPrimaryButtonDown()){
					addState(mouseEvent.getX(),mouseEvent.getY());
				
				}
				// If it is a primary click, close the context menu
				if (mouseEvent.isPrimaryButtonDown()){
					rightClickMenu.hide();
					
				// Open the context menu on a right click
				} else if (mouseEvent.isSecondaryButtonDown()) {
					rightClickMenu.show(pane, mouseEvent.getScreenX(), mouseEvent.getScreenY());
				}
			}
		});
		
		// Catch released drags and reset the drag source so a transition is not added
		pane.setOnMouseDragReleased(new EventHandler<MouseDragEvent>() {
		    @Override public void handle(MouseDragEvent event) {
				if (getInteractState() == InteractState.CREATE){
					dragSource = null;
					pane.getScene().setCursor(Cursor.DEFAULT);
		    	}
				event.consume();
		    }
		});
		
	}
	
	/**
	 * Enables the pane on which the Automata is displayed to expand as needed
	 */
	private void enablePaneExpansion(){
		
		// Expands the pane to fit the scroll pane if it can be shrunk
		// Always expands to fill the screen where possible.
		editor.getScrollPane().widthProperty().addListener(new ChangeListener<Number>() {
		    @Override public void changed(ObservableValue<? extends Number> observableValue, Number oldWidth, Number newWidth) {
		        pane.setMinWidth((double) newWidth - 10);
		    }
		});
		
		editor.getScrollPane().heightProperty().addListener(new ChangeListener<Number>() {
		    @Override public void changed(ObservableValue<? extends Number> observableValue, Number oldHeight, Number newHeight) {
		        pane.setMinHeight((double) newHeight - 10);
		    }
		});
	}
	
	/**
	 * When serializing, writes objects in a format that can be read back and used 
	 * to reconstruct a deep copy of the object itself. 
	 * @param out		output stream to write to
	 * @throws IOException
	 */
	private void writeObject(java.io.ObjectOutputStream out) throws IOException{
		out.defaultWriteObject();

		out.writeObject(automata);
		out.writeObject(stateMap);
		out.writeObject(transitionMap);
	}
	
	/**
	 * When deserializing, reads objects in a proper order to reconstruct a deep 
	 * copy of the automata controller.
	 * @param in
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("unchecked")
	private void readObject(java.io.ObjectInputStream in)
			throws IOException, ClassNotFoundException{
		
		try {
			
			in.defaultReadObject();
			undoManager = new UndoManager();
			pane = new Pane();
			
			automata = (FiniteAutomata) in.readObject();
			stateMap = (HashMap<State,StateControl>) in.readObject();
			transitionMap = (HashMap<Transition,TransitionControl>) in.readObject();
			
			for (StateControl s : stateMap.values()){
				pane.getChildren().addAll(s.getVisuals());
				s.updateVisuals();
			}
			for (TransitionControl t : transitionMap.values())
				pane.getChildren().addAll(t.getVisuals());
			
			sortVisuals();
			enableInteraction();
			
		} catch (ClassCastException e) {
			throw new ClassCastException("The read class was different than expected.");
		}
		
	}
	
	
	
	/**
	 * An action for adding states.
	 * @author ncameron
	 * @author kmcdonell
	 */
	private class AddStateAction implements ActionPattern {

		private final StateControl stateControl;
		
		public AddStateAction(StateControl stateControl){
			this.stateControl = stateControl;
		}
		
		@Override public void redo() {
			AutomataControl.this.addStateWithoutAction(stateControl);			
		}
		@Override public void undo() {
			AutomataControl.this.removeStateWithoutAction(stateControl);		
		}
		
	}


	/**
	 * An action for removing states.
	 * @author ncameron
	 * @author kmcdonell
	 */
	private class RemoveStateAction implements ActionPattern {

		private final StateControl stateControl;
		private final List<TransitionControl> removedTransitions;
		
		public RemoveStateAction(StateControl stateControl, List<TransitionControl> removedTransitions){
			this.stateControl = stateControl;
			this.removedTransitions = removedTransitions;
		}
		
		@Override public void redo() {
			AutomataControl.this.removeStateWithoutAction(stateControl);
			for (TransitionControl t : removedTransitions)
				AutomataControl.this.removeTransitionWithoutAction(t);
		}
		@Override public void undo() {
			AutomataControl.this.addStateWithoutAction(stateControl);
			for (TransitionControl t : removedTransitions)
				AutomataControl.this.addTransitionWithoutAction(t);
		}
		
	}

	/**
	 * An action for adding transitions.
	 * 
	 * @author ncameron
	 * @author kmcdonell
	 */
	private class AddTransitionAction implements ActionPattern {

		private final TransitionControl transitionControl;
		
		public AddTransitionAction(TransitionControl transitionControl){
			this.transitionControl = transitionControl;
		}
		
		@Override
		public void redo() {
			AutomataControl.this.addTransitionWithoutAction(transitionControl);			
		}
		@Override public void undo() {
			AutomataControl.this.removeTransitionWithoutAction(transitionControl);		
		}
		
	}

	/**
	 * An action for removing transitions.
	 * @author ncameron
	 * @author kmcdonell
	 */
	private class RemoveTransitionAction implements ActionPattern {

		private final TransitionControl transitionControl;
		
		public RemoveTransitionAction(TransitionControl transitionControl){
			this.transitionControl = transitionControl;
		}
		
		@Override public void redo() {
			AutomataControl.this.removeTransitionWithoutAction(transitionControl);			
		}
		@Override public void undo() {
			AutomataControl.this.addTransitionWithoutAction(transitionControl);		
		}
		
	}

	/**
	 * An action for visual repositioning.
	 * @author ncameron
	 * @author kmcdonell
	 */
	private class VisualRepositionAction implements ActionPattern {
		
		private final Map<StateControl,Point2D.Double> oldSPos;
		private final Map<TransitionControl,Point2D.Double> oldTPos;
		private final Map<StateControl,Point2D.Double> newSPos;
		private final Map<TransitionControl,Point2D.Double> newTPos;
		
		public VisualRepositionAction(Map<StateControl,Point2D.Double> oldSPos,
					Map<TransitionControl,Point2D.Double> oldTPos,
					Map<StateControl,Point2D.Double> newSPos,
					Map<TransitionControl,Point2D.Double> newTPos){
			this.oldSPos = oldSPos;
			this.oldTPos = oldTPos;
			this.newSPos = newSPos;
			this.newTPos = newTPos;
		}
		
		/**
		 * Returns all states and transitions moved to their new positions
		 */
		@Override public void redo() {
			if (newSPos != null){
				for (Entry<StateControl,Point2D.Double> entry : newSPos.entrySet())
					entry.getKey().setStatePosition(entry.getValue());
			}
			if (newTPos != null){
				for (Entry<TransitionControl,Point2D.Double> entry : newTPos.entrySet())
					entry.getKey().setAnchorPosition(entry.getValue());
			}
			
		}
		/**
		 * Returns all states and transitions moved to their original positions
		 */
		@Override public void undo() {
			if (oldSPos != null) {
				for (Entry<StateControl,Point2D.Double> entry : oldSPos.entrySet())
					entry.getKey().setStatePosition(entry.getValue());
			}
			if (oldTPos != null) {
				for (Entry<TransitionControl,Point2D.Double> entry : oldTPos.entrySet())
					entry.getKey().setAnchorPosition(entry.getValue());
			}
		}
		
	}

	
	
	
	
}