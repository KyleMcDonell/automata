package falt.controller;

import java.awt.geom.Point2D;
import java.io.IOException;
import java.io.Serializable;

import falt.model.State;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ContextMenu;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.StrokeType;

/**
 * Defines a controller for the states in the automata.
 * 
 * @author ncameron
 * @author kmcdonell
 */
public class StateControl implements Serializable {

	/** Serialization ID */
	private static final long serialVersionUID = 7514031965012742619L;
	/** The state that this controller is responsible for. */
	private State state;
	/** The automata controller to which this state controller belongs. */
	private AutomataControl automata;
	/** Visual representation of the state. */
	private transient ObservableList<Node> visuals;
	// ContextMenu to allow users to mark the state as the start or as an accept state
	private transient ContextMenu rightClickMenu;
	
	/**
	 * Creates a new state controller from a given position, state, and automata 
	 * controller.
	 * 
	 * @param x		the x coordinate of the position
	 * @param y		the y coordinate of the position
	 * @param state	the State to be controlled
	 * @param aut	the AutomataControl this StateControl serves
	 */
	public StateControl(double x, double y, State state, AutomataControl aut){
		this.state = state;
		this.automata = aut;
		
		// Initialize the circle and other visual components
		createVisuals(x,y);
		enableInteraction();
		enableContextMenu();
		updateVisuals();
	}
	
	/** Returns the state that this controller is responsible for. */
	public State getState(){  return state;	}
	
	/** Returns the x coordinate of the state in the view. */
	public double getX(){ return getStateVisual().getCenterX(); }
	/** Returns the y coordinate of the state in the view. */
	public double getY(){ return getStateVisual().getCenterY(); }

	/** Sets the x coordinate of the state in the view to the given value. */
	public void setX(double x){ getStateVisual().setCenterX(x); }
	/** Sets the y coordinate of the state in the view to the given value. */
	public void setY(double y){ getStateVisual().setCenterY(y); }
	
	/** Returns a list of visuals which represent this state. */
	public ObservableList<Node> getVisuals(){ return visuals; }
	
	/** Returns the visual which represents the circle. */
	public Circle getStateVisual(){ return (Circle) visuals.get(0); }
	
	/** Returns a point representation of the state's position. */
	public Point2D.Double getStatePosition(){
		return new Point2D.Double(getStateVisual().getCenterX(),
				getStateVisual().getCenterY());
	}
	
	/** Set the state's visual position to the given point. */
	public void setStatePosition(Point2D.Double point){
		setX(point.getX());
		setY(point.getY());
	}
	
	
	/**
	 * Refreshes the visibility of the additional visual items, acceptance and
	 * start marker.
	 */
	public void updateVisuals(){
		// Set the acceptRing to show only if the state is an accept state
		visuals.get(1).setVisible(state.isAccept());	
		
		// Set the start arrow to show only if it is the start state
		boolean isStart = automata.getStartState() == this;
		visuals.get(2).setVisible(isStart);
		// Set the checkMenu to reflect whether this state is a start state
		( (CheckMenuItem)rightClickMenu.getItems().get(1) )
			.selectedProperty().set(isStart);
	}
	
	/**
	 * Highlights the visual representation of this state controller.
	 * @param highlight		whether or not to highlight
	 */
	public void highlight(boolean highlight){
		if (highlight){
			getStateVisual().setFill(Color.gray(.45));
		} else {
			getStateVisual().setFill(Color.gray(.75));
		}
	}
	
	/**
	 * Initializes the visual elements of the state controller at the given
	 * position.
	 * 
	 * @param x		the x-coord of the position
	 * @param y		the y-coord of the position
	 */
	private void createVisuals(double x, double y){
		
		int radius = 20;
		Color stateColor = Color.gray(.75);
		Color outlineColor = Color.BLACK;
		
		Circle circle;
		Circle acceptRing;
		Polygon startTriangle;
		
		// Create a circle to represent the state
		circle = new Circle(x,y,radius);
		circle.setFill(stateColor);
	    circle.setStroke(outlineColor);
	    circle.setStrokeWidth(2);
	    circle.setStrokeType(StrokeType.OUTSIDE);
	    
	    // Create a ring to indicate the state is an acceptState
	    acceptRing = new Circle(radius*.75);
	    acceptRing.setFill(null);
		acceptRing.setStrokeType(StrokeType.INSIDE);
		acceptRing.setStroke(outlineColor);
		acceptRing.setStrokeWidth(2);
		acceptRing.centerXProperty().bind(circle.centerXProperty());
		acceptRing.centerYProperty().bind(circle.centerYProperty());
	    acceptRing.setMouseTransparent(true);
	    acceptRing.setVisible(state.isAccept());	
	    
	    // Create a triangle to the left of the state to indicate it is the start state
	    startTriangle = new Polygon(x-radius, y,
	    		x-radius-Math.tan(45)*radius, y+radius,
	    		x-radius-Math.tan(45)*radius, y-radius );
	    ObservableList<Double> points = startTriangle.getPoints();
	    
	    // Set the triangle to move with the state
	    circle.centerXProperty().addListener(new ChangeListener<Number>() {
	    	@Override public void changed(ObservableValue<? extends Number> ov, Number oldX, Number newX){
	    		points.set(0, (double) newX-radius);
	    		points.set(2, (double) newX-Math.tan(45)*radius-radius);
	    		points.set(4, (double) newX-Math.tan(45)*radius-radius);
	    	}
	    });
	    
	    circle.centerYProperty().addListener(new ChangeListener<Number>() {
	    	@Override public void changed(ObservableValue<? extends Number> ov, Number oldY, Number newY){
	    		points.set(1,(double) newY);
	    		points.set(3,(double) newY+radius);
	    		points.set(5,(double) newY-radius);
	    	}
	    });
	    
	    startTriangle.setFill(stateColor);
	    startTriangle.setStrokeType(StrokeType.INSIDE);
		startTriangle.setStroke(outlineColor);
		startTriangle.setStrokeWidth(2);
	    
		
		visuals = FXCollections.observableArrayList(circle,acceptRing,startTriangle);
		
	}
	
	/**
	 * Creates event handlers for updating the view.
	 */
	private void enableInteraction() {
		
		Circle circle = this.getStateVisual();
		
		final Point2D.Double dragOffset = new Point2D.Double();
		final Point2D.Double dragStart = new Point2D.Double();
		final BooleanProperty moved = new SimpleBooleanProperty(false);
		
		// When we are clicked do this
		circle.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override public void handle(MouseEvent mouseEvent) {
				
				InteractState state = automata.getInteractState();
				
				// Allow the state to be moved if the window is in mouse or testing mode
				if (state == InteractState.MOUSE || state == InteractState.TESTING){
					if (mouseEvent.isPrimaryButtonDown()){
						// record a delta distance for the drag and drop operation.
						dragOffset.x = circle.getCenterX() - mouseEvent.getX();
						dragOffset.y = circle.getCenterY() - mouseEvent.getY();
						dragStart.x = circle.getCenterX();
						dragStart.y = circle.getCenterY();
						circle.getScene().setCursor(Cursor.CLOSED_HAND);
					}
				}
				
				// If in mouse mode, right click should open the context menu
				if (state == InteractState.MOUSE){
					if (mouseEvent.isSecondaryButtonDown()) {
						// Open edit window to rename, ect.
						rightClickMenu.show(circle, mouseEvent.getScreenX(), mouseEvent.getScreenY());
					}
					
				} else if (state == InteractState.DELETE){
					automata.removeState(StateControl.this);
				}
				mouseEvent.consume();
				
			}
		});
		
		
		// When the circle is dragged in mouse mode, move the circle
		circle.setOnMouseDragged(new EventHandler<MouseEvent>() {
			@Override public void handle(MouseEvent mouseEvent) {
		
				InteractState state = automata.getInteractState();
				if (state == InteractState.MOUSE || state == InteractState.TESTING){
					if (mouseEvent.isPrimaryButtonDown()){
						moved.set(true);
						double newX = mouseEvent.getX() + dragOffset.x;
						if (newX > 0 && newX < circle.getScene().getWidth()) {
							circle.setCenterX(newX);
						}
						double newY = mouseEvent.getY() + dragOffset.y;
						if (newY > 0 && newY < circle.getScene().getHeight()) {
							circle.setCenterY(newY);
						}
					}
				}
				mouseEvent.consume();
				
			}
		});
		
		
		// When the mouse is released, set the cursor to an open hand and add a move action
		circle.setOnMouseReleased(new EventHandler<MouseEvent>() {
			@Override public void handle(MouseEvent mouseEvent) {
				
				InteractState state = automata.getInteractState();
				if (state == InteractState.MOUSE || state == InteractState.TESTING){
					circle.getScene().setCursor(Cursor.OPEN_HAND);
					
					// If it was moved, add a move action
					if (moved.get()){
						double dx = circle.getCenterX()-dragStart.x;
						double dy = circle.getCenterY()-dragStart.y;
						if (Math.abs(dx) > 0.1 || Math.abs(dy) > 0.1)
							automata.getUndoManager().addAction(new CircleMovementAction(circle,dx,dy));
						moved.set(false);
					}
				}
				mouseEvent.consume();

			}
		 });
		
		
		
		
		// When a drag is started in transition mode, start a drag event
		circle.setOnDragDetected(new EventHandler<MouseEvent>() {
		    @Override public void handle(MouseEvent event) {
		    	
		    	InteractState state = automata.getInteractState();
				if (state == InteractState.CREATE){
				    	circle.getScene().setCursor(Cursor.MOVE);
				    	circle.getScene().startFullDrag();
				        automata.setDragSource(StateControl.this);
		    	}
				event.consume();
		    }
		});
		
		
		
		// If a drag is dropped in transition, add a transition if it comes from a
		// State wrapper.  This currently doesn't work!!  GestureSource is Circle class!
		circle.setOnMouseDragReleased(new EventHandler<MouseDragEvent>() {
		    @Override public void handle(MouseDragEvent event) {
		    	
		    	InteractState state = automata.getInteractState();
				if (state == InteractState.CREATE){
					circle.getScene().setCursor(Cursor.DEFAULT);
					automata.dragEnded(StateControl.this);
		    	}
				event.consume();
		    }
		});
		
		
		
		// Change the cursor symbol to reflect the interaction mode
		circle.setOnMouseEntered(new EventHandler<MouseEvent>() {
			@Override public void handle(MouseEvent mouseEvent) {
				
				InteractState state = automata.getInteractState();
				if (state == InteractState.MOUSE || state == InteractState.TESTING){
					if (!mouseEvent.isPrimaryButtonDown()) {
						circle.getScene().setCursor(Cursor.OPEN_HAND);
					}
				} else if (state == InteractState.DELETE){
					if (!mouseEvent.isPrimaryButtonDown()) {
						circle.getScene().setCursor(Cursor.HAND);
					}
				}
				mouseEvent.consume();
			}
		});
		 
		// Reset the cursor when the mouse leaves the circle
		circle.setOnMouseExited(new EventHandler<MouseEvent>() {
			@Override public void handle(MouseEvent mouseEvent) {
				
				InteractState state = automata.getInteractState();
				if (state == InteractState.MOUSE || state == InteractState.TESTING){
					if (!mouseEvent.isPrimaryButtonDown()) {
						circle.getScene().setCursor(Cursor.DEFAULT);
						
					}
				} else if (state == InteractState.DELETE){
					if (!mouseEvent.isPrimaryButtonDown()) {
						circle.getScene().setCursor(Cursor.DEFAULT);
					}
				}
				mouseEvent.consume();
			}
		});
	
	}
	
	/**
	 * Initializes the right click menu.
	 */
	private void enableContextMenu(){
		
		// create the right click menu
		rightClickMenu = new ContextMenu();
		
		CheckMenuItem acceptMenuItem = new CheckMenuItem("Make Accept State");
		acceptMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e){
				
				// Flip the accept state and update visuals
				boolean wasAccept = state.isAccept();
				acceptMenuItem.selectedProperty().set(!wasAccept);
				state.setAccept(!wasAccept);
				updateVisuals();

				// Add this action to the undoManager
				automata.getUndoManager().addAction(
						new acceptStateChange(acceptMenuItem));
				
			}
		});
		
		CheckMenuItem startMenuItem = new CheckMenuItem("Make Start State");
		startMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e){
				
				// Change the accept state, automatically updating visuals of
				// this state and the old start state
				StateControl oldStart = automata.getStartState();
				if (oldStart == StateControl.this){
					automata.setStartState(null);
				} else {
					automata.setStartState(StateControl.this);
				}
				
				// Add this action to the undoManager
				automata.getUndoManager().addAction(
						new startStateChange(oldStart, StateControl.this));
				
			}
		});
		rightClickMenu.getItems().add(acceptMenuItem);
		rightClickMenu.getItems().add(startMenuItem);	
		
	}
	
	
	/**
	 * Writes the object to a given output stream.
	 * @param out				the output stream to write to
	 * @throws IOException
	 */
	private void writeObject(java.io.ObjectOutputStream out) throws IOException{
		out.defaultWriteObject();
		out.writeObject(getStatePosition());
	}
 
	/**
	 * Reads and constructs an object from a given input stream.
	 * @param in				the input stream to read from
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private void readObject(java.io.ObjectInputStream in)
			throws IOException, ClassNotFoundException{
		in.defaultReadObject();
		Point2D.Double p = (Point2D.Double) in.readObject();
		createVisuals(p.getX(),p.getY());
		enableInteraction();
		enableContextMenu();
	}
	
	/**
	 * Defines a class for the state to allow undo/redo functionality with 
	 * changes of whether the state is an accept state.
	 * 
	 * @author ncameron
	 * @author kmcdonell
	 */
	private class acceptStateChange implements ActionPattern{
		
		private CheckMenuItem acceptMenuItem;
		
		public acceptStateChange(CheckMenuItem acceptMenuItem){
			this.acceptMenuItem = acceptMenuItem;
		}
		
		@Override public void redo() {
			undo();
		}
		@Override public void undo() {
			boolean wasAccept = state.isAccept();
			acceptMenuItem.selectedProperty().set(!wasAccept);
			state.setAccept(!wasAccept);
			updateVisuals();
		}
		
	}
	
	/**
	 * Defines a class for the state to allow undo/redo functionality with 
	 * changes of whether the state is a start state.
	 * 
	 * @author ncameron
	 * @author kmcdonell
	 */
	private class startStateChange implements ActionPattern{
		
		private StateControl oldStart, newStart;
		
		public startStateChange(StateControl oldStart, StateControl newStart){
			this.oldStart = oldStart;
			this.newStart = (newStart != oldStart) ? newStart : null;
		}
		
		@Override public void redo() {
			automata.setStartState(newStart);
		}
		@Override public void undo() {
			automata.setStartState(oldStart);
		}
		
	}

    
}
