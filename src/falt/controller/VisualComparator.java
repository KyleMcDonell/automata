package falt.controller;

import java.util.Comparator;
import java.util.HashMap;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.shape.Arc;
import javafx.scene.shape.Circle;

/**
 * Defines a comparator for the visual elements, to ensure that the most important
 * for the user to be able to interact with are shown on top of others.
 * 
 * @author ncameron
 * @author kmcdonell
 */
public class VisualComparator implements Comparator<Node> {
	
		/** Mapping between classes and their priority in integer form */
		private static HashMap<Class<?>,Integer> CLASS_PRIORITY;
		/** The single instance of this comparator */
		private static VisualComparator INSTANCE;
		
		/**
		 * Initialization block for the visual priority of different nodes.
		 */
		static {
			CLASS_PRIORITY = new HashMap<>();
			CLASS_PRIORITY.put(TextField.class,5);
			CLASS_PRIORITY.put(Label.class,4);
			CLASS_PRIORITY.put(Anchor.AnchorCircle.class,3);
			CLASS_PRIORITY.put(Circle.class,2);
			CLASS_PRIORITY.put(Arc.class,1);
			
			INSTANCE = new VisualComparator();
		}
		
		/**
		 * Private constructor so the class is never created
		 */
		private VisualComparator(){};
		
		/**
		 * Obtains the single instance of this comparator
		 * @return		the instance of this comparator
		 */
		public static VisualComparator getInstance(){
			return INSTANCE;
		}
		
		/*
		 * (non-Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		@Override public int compare(Node o1, Node o2) {
			Integer v1 =  CLASS_PRIORITY.get(o1.getClass());
			Integer v2 =  CLASS_PRIORITY.get(o2.getClass());
			
			v1 = (v1 != null) ? v1 : Integer.MIN_VALUE;
			v2 = (v2 != null) ? v2 : Integer.MIN_VALUE;
			
			return Integer.compare(v1, v2);
		}
		
		
	
}
