package falt.controller;

/**
 * Defines the interaction states for the editor. 
 * 
 * MOUSE is for moving states, transitions, and adjusting properties of states, 
 * as well as editing transition characters. 
 * 
 * CREATE is for creating states and transitions. 
 * 
 * DELETE is for deleting states and transitions.
 * 
 * TESTING is for testing the automata, and prevents editing.
 * 
 * @author ncameron
 * @author kmcdonell
 */
public enum InteractState {

	MOUSE, CREATE, DELETE, TESTING;
	
}
