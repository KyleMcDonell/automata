package falt.controller;

import java.awt.geom.Point2D;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import falt.MainApp;
import falt.model.AutomataCorrespondence;
import falt.model.AutomataOperations;
import falt.model.State;
import falt.model.Transition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * Class to control the applications root pane.  It handles all menu bar items and
 * manages the editor tabs.
 * 
 * @author ncameron
 * @author kmcdonell
 */
public class MenuController implements Initializable { 
	
	/** Reference to the stage the menu controller resides on */
	private Stage stage;
	/** Map between the tabs and the automatas they hold */
	private HashMap<Tab, AutomataControl> tabs = new HashMap<>();
	
	@FXML private TabPane tabPane;
	
	// File menu items
	@FXML private MenuItem newAutomata;
	@FXML private MenuItem open;
	@FXML private MenuItem save;
	@FXML private MenuItem saveAs;
	@FXML private MenuItem saveAsImage;
	@FXML private MenuItem quit;
	@FXML private MenuItem help;
	@FXML private MenuItem about;
	
	// Compare menu items
	@FXML private MenuItem checkDeterminism;
	@FXML private MenuItem checkEmptiness;
	@FXML private MenuItem checkEquivalence;
	@FXML private MenuItem minimizeDFA;
	@FXML private MenuItem convertToDFA;
	@FXML private MenuItem union;
	@FXML private MenuItem intersect;
	@FXML private MenuItem complement;
	@FXML private MenuItem concatenation;
	@FXML private MenuItem kleeneStar;
	
	/**
	 * Initializes the menu controller by adding keyboard shortcuts and 
	 * disabling unimplemented or unavailable menu items.
	 */
	@Override
	public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
		// set keycode shortcuts, e.g. ctrl/apple-n to create a new tab
		newAutomata.setAccelerator(new KeyCodeCombination(KeyCode.N, KeyCombination.SHORTCUT_DOWN));
		open.setAccelerator(new KeyCodeCombination(KeyCode.O, KeyCombination.SHORTCUT_DOWN));
		save.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.SHORTCUT_DOWN));
		saveAs.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.SHORTCUT_DOWN, KeyCombination.SHIFT_DOWN));
		saveAsImage.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.SHORTCUT_DOWN, KeyCombination.ALT_DOWN));
		quit.setAccelerator(new KeyCodeCombination(KeyCode.Q, KeyCombination.SHORTCUT_DOWN));
		
		// Disable saving as no tabs are open
		save.setDisable(true);
		saveAs.setDisable(true);
		saveAsImage.setDisable(true);
		
		// Currently not implemented:
		convertToDFA.setDisable(true);
		minimizeDFA.setDisable(true);
		help.setDisable(true);
		about.setDisable(true);
		
		// Allow saving only if there is an open tab
		tabPane.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>(){
			@Override
			public void changed(ObservableValue<? extends Tab> arg0, 
					Tab oldValue, Tab newValue){
				
				// Currently only saveAs is implemented
				boolean noTab = newValue == null;
				save.setDisable(noTab);
				saveAs.setDisable(noTab);
				//saveAsImage.setDisable(noTab);
				
			}
		});
	}
	
	/**
	 * Sets the stage the menu controller resides on.
	 * @param stage		the stage the menu controller resides on
	 */
	public void setStage(Stage stage){
		this.stage = stage;
	}
	
	
	// Event Listener on MenuItem[#newAutomata].onAction
	/**
	 * Creates a new untitled automata tab
	 */
	@FXML public void newAutomataTab(ActionEvent event) {
		newAutomataTab(null, "Untitled*");
	}
	
	
	/**
	 * Creates a new automata tab.
	 * 
	 * @param aut		the automata controller for which the tab should be created. 
	 * 					If null, a new empty automata is created
	 * @param fileName	the name of the file in which the automata is stored
	 */
	public void newAutomataTab(AutomataControl aut, String fileName) {
		
		try {
            // Load the automata window
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/AutomataEditor.fxml"));
            AnchorPane automataEditor = (AnchorPane) loader.load();
            EditorController editor = (EditorController)loader.getController();
            
            if (aut == null)
            	aut = new AutomataControl();
            
            aut.setEditor(editor);
            editor.getScrollPane().setContent(aut.getViewPane());
            editor.setAutomata(aut);
            UndoManager undoManager = aut.getUndoManager();
            
            // Adds the new automata window
            Tab tab = new Tab();
            tab.setText(fileName);
            tab.setContent(automataEditor);
            tabs.put(tab, aut);
            tabPane.getTabs().add(tab);
            tabPane.getSelectionModel().select(tab);
            
            // Add a listener to update the tab saving
            undoManager.savedProperty().addListener(new ChangeListener<Boolean>(){
				@Override
				public void changed(ObservableValue<? extends Boolean> arg0, 
						Boolean oldValue, Boolean newValue){
					
					// If the undoManager is saved and there is an asterisk at the end 
					// of the file, remove it from the filename
					if (newValue){
						tab.setText(tabNameWithoutAsterisk(tab));
						
					// If unsaved, add an asterisk to the end of the fileName
					} else {
						tab.setText(tabNameWithoutAsterisk(tab) + '*');
					}
				}
			});
            
            // Add a listener to warn the user that a tab is unsaved
            tab.setOnCloseRequest(new EventHandler<Event>() {
    			@Override public void handle(Event e) {
    				
    				if (!undoManager.getSaved()){
    					Alert alert = new Alert(AlertType.CONFIRMATION);
    					alert.setTitle("Unsaved data will be lost");
    					alert.setHeaderText("You have unsaved changes.  "
    							+ "Are you sure you want to close this tab?");
    					alert.setContentText("Click OK to continue or cancel to return.");
    					alert.showAndWait();
    					
    					// Close the tab if OK is pressed
    					if (alert.getResult() == ButtonType.OK){
    			            tabs.remove(tab);
    			            tabPane.getTabs().remove(tab);
    			            
    			        // Otherwise consume the event to prevent it from being closed
    					} else {
    						e.consume();
    					}
    				}
    				
    			}
    		});
            
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

	
	// Event Listener on MenuItem[#open].onAction
	/**
	 * Opens a file chooser and allows the user to load an automata into the program.
	 * This creates a new tab for the automata with the name of the file.
	 */
	@FXML public void openFile(ActionEvent event) {
		
		// Create a new file chooser for finite automata files
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Save Finite Automata");
		String defaultDirLoc = System.getProperty("user.home");
		File defaultDirectory = new File(defaultDirLoc);
		if(!defaultDirectory.canRead()) {
			defaultDirectory = new File("c:/");
		}
		fileChooser.setInitialDirectory(defaultDirectory);
		fileChooser.getExtensionFilters().addAll(
				new FileChooser.ExtensionFilter("Finite Automata", "*.fa"),
				new FileChooser.ExtensionFilter("All Files", "*.*"));
		
		// Load the file selected
		File f = fileChooser.showOpenDialog(stage);
		if (f != null){
			try {
		
				ObjectInputStream in = new ObjectInputStream(new FileInputStream(f));
			    AutomataControl aut = (AutomataControl) in.readObject();
			    in.close();
			    
			    // If the file name ends with .fa, remove that from the tab name
			    if (f.getName().toLowerCase().endsWith(".fa")){
			    	newAutomataTab(aut,f.getName().substring(0,f.getName().length()-3));	
			    } else {
			    	newAutomataTab(aut,f.getName());	
			    }
			    
			    // Set the automata as saved and set its file location
			    aut.setFile(f);
			    aut.getUndoManager().save();
			    
			} catch (IOException ex) {
                System.out.println(ex.getMessage());
			} catch (ClassNotFoundException ex) {
				ex.printStackTrace();
			} catch (ClassCastException ex) {
				throw new ClassCastException("The loaded file was not an automata");
			}
			
		}
			    
	}
	
	// Event Listener on MenuItem[#save].onAction
	/**
	 * Saves the current tab's automata into its file.  If
	 * no file for it currently exists, saveAs is called instead.
	 */
	@FXML public void saveFile(ActionEvent event) {
		AutomataControl aut = tabs.get(tabPane.getSelectionModel()
    			.getSelectedItem());
		
		// If the file has never been saved, Save As
		if (aut.getFile() == null){
			saveFileAs(null);
			
		// Otherwise, save it to its file with no rewrite warning
		} else {
			try {
            	// Dangerous, overwrite not confirmed!
            	ObjectOutputStream out = new ObjectOutputStream(
            			new FileOutputStream(aut.getFile()));
            	out.writeObject(aut);
            	out.close();
            	
            	// Indicate the automata has been saved
            	aut.getUndoManager().save();
            	
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
			
		}
		
	}
	
	// Event Listener on MenuItem[#saveAs].onAction
	/**
	 * Opens a file chooser to allow the user to save the current
	 * tab's automata into a new file.
	 */
	@FXML public void saveFileAs(ActionEvent event) {
		
		// Get the tab to be saved
		Tab tab = tabPane.getSelectionModel().getSelectedItem();
		
		// Create a new file chooser saving finite automata files
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Save Finite Automata");
		String defaultDirLoc = System.getProperty("user.home");
		File defaultDirectory = new File(defaultDirLoc);
		if(!defaultDirectory.canRead()) {
			defaultDirectory = new File("c:/");
		}
		fileChooser.setInitialDirectory(defaultDirectory);
		fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("Finite Automata", "*.fa"));
		
		// Set the initial file name to be the tab name with no asterisk 
		// if the tab is not untitled
		String name = tabNameWithoutAsterisk(tab);
		if (!name.equals("Untitled"))
			fileChooser.setInitialFileName(tabNameWithoutAsterisk(tab) + ".fa");
		
	
		// Get the chosen file
		File f = fileChooser.showSaveDialog(stage);
		if (f != null) {
            try {
            	
            	// Add a .fa to the end of the file if it doesn't already exist
            	// Dangerous, overwrite not confirmed!
            	if (!f.getPath().toLowerCase().endsWith(".fa"))
            		f = new File(f.getAbsolutePath() + ".fa");
            	
            	ObjectOutputStream out = new ObjectOutputStream(
            			new FileOutputStream(f));
            	AutomataControl aut = tabs.get(tabPane.getSelectionModel()
            			.getSelectedItem());
            	out.writeObject(aut);
            	out.close();
            	
            	// Set the automata to be saved and indicate its file location
            	aut.setFile(f);
            	aut.getUndoManager().save();
            	
            	// Sets the tab text to reflect the file name without the extension
            	// if the extension is .fa
            	if (f.getName().endsWith(".fa"))
            		tab.setText(f.getName().substring(0,f.getName().length()-3));
            	else
            		tab.setText(f.getName());
            	
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
	}
	
	// Event Listener on MenuItem[#saveAsImage].onAction
	/**
	 * Unimplemented, saves a screenshot of the current tab
	 */
	@FXML public void saveImage(ActionEvent event) {
		// TODO Autogenerated
	}
	
	// Event Listener on MenuItem[#quit].onAction
	/**
	 * Exits the program.  Note this does not prompt the user to save 
	 * any unsaved work.
	 */
	@FXML public void quit(ActionEvent event) {
		stage.close();
	}
	
	// Event Listener on MenuItem[#help].onAction
	/**
	 * Unimplemented.  Opens a help menu explaining the program.
	 */
	@FXML public void openHelp(ActionEvent event) {
		// TODO Autogenerated
	}
	
	// Event Listener on MenuItem[#about].onAction
	/**
	 * Unimplemented.  Opens a window with the program's author information
	 */
	@FXML public void openAbout(ActionEvent event) {
		// TODO Autogenerated
	}
	
	/**
	 * Shows an alert popup with the specified header, title, and message.
	 * 
	 * @param popupHeader	the header for the popup
	 * @param popupTitle	the title for the popup
	 * @param popupMessage	the message for the popup
	 */
	public void showPopup(String popupHeader, String popupTitle, String popupMessage){
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle(popupHeader);
		alert.setHeaderText(popupTitle);
		alert.setContentText(popupMessage);
		alert.showAndWait();
	}
	
	
	/**
	 * Prompts the user to select two different tabs for the provided
	 * operation. If the pane does not have two tabs, this displays a popup
	 * warning.
	 * 
	 * @param operation 	the string name of the operation to be used in the
	 * 						user prompt
	 * @param operationVerb	the string verb of the operation, also used in the 
	 * 						user prompt
	 * @return 				an array with the first and second choice tabs or null
	 * 						if the user aborted the selection
	 */
	public Tab[] pickTwo(String operation, String operationVerb){
		
		Tab[] twoAutomata = new Tab[2];
		
		// Create a list of tabNames in the order the tabs appear
		ObservableList<String> tabNames = FXCollections.observableList(new ArrayList<>());
		ObservableList<Tab> tabList = tabPane.getTabs();
		for (Tab tab : tabList)
			tabNames.add(tabNameWithoutAsterisk(tab));
		
		
		// If there are not two tabs, popup a window warning the user
		if (tabList.size()<2){
			showPopup("Warning","Not enough automata to " + operationVerb + ".",
					"You must have at least two automata open." );
			return null;
		}
		
		// Creates a dialog to select two tabs
		ChoiceBox<String> dialog1 = new ChoiceBox<String>(tabNames);
		ChoiceBox<String> dialog2 = new ChoiceBox<String>(tabNames);

		dialog1.setPrefWidth(200);
		dialog2.setPrefWidth(200);
		dialog1.setValue(tabNames.get(0));
		dialog2.setValue(tabNames.get(1));
		
		GridPane gridPane = new GridPane();
		gridPane.setHgap(10);
		gridPane.setVgap(5);
		gridPane.add(dialog1, 1, 1);
		gridPane.add(dialog2, 1, 2);

		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle(operation);
		alert.setHeaderText("Pick two distinct automata.");
		alert.getDialogPane().setContent(gridPane);
		
		// Get the select tabs if OK is selected and return the two
		// automatas in the tabs
		alert.showAndWait();
		if (alert.getResult() == ButtonType.OK){
			int index1 = tabNames.indexOf((dialog1.getValue()));
			int index2 = tabNames.indexOf((dialog2.getValue()));
			twoAutomata[0] = tabList.get(index1);
			twoAutomata[1] = tabList.get(index2);
			
			while (twoAutomata[0] == twoAutomata[1]){
				Alert mustBeDistinct = new Alert(AlertType.WARNING);
				mustBeDistinct.setHeaderText("Choices not distinct");
				mustBeDistinct.setContentText("You must choose two distinct finite "
						+ "automata to perform " + operationVerb + ".");
				mustBeDistinct.showAndWait();
				alert.showAndWait();
				if (alert.getResult() == ButtonType.CANCEL){
					return null;
				}
				index1 = tabNames.indexOf((dialog1.getValue()));
				index2 = tabNames.indexOf((dialog2.getValue()));
				twoAutomata[0] = tabList.get(index1);
				twoAutomata[1] = tabList.get(index2);
			}
			return twoAutomata;
		} else {
			return null;
		}
		
	}
	
	/**
	 * Prompts the user to select a tab for the provided operation.  This
	 * displays a warning if the tab pane is empty.
	 * 
	 * @param operation 	the string name of the operation to be used in the
	 * 						user prompt
	 * @param operationVerb	the string verb of the operation, also used in the 
	 * 						user prompt
	 * @return 				the selected tab or null if the user aborted the selection
	 */
	public Tab pickOne(String operation, String operationVerb){

		Tab automata;
		
		// Create a list of tabNames in the order the tabs appear
		ObservableList<String> tabNames = FXCollections.observableList(new ArrayList<>());
		ObservableList<Tab> tabList = tabPane.getTabs();
		for (Tab tab : tabList)
			tabNames.add(tabNameWithoutAsterisk(tab));
		
		
		// If there are no tabs, popup a window warning the user
		if (tabList.size()<1){
			showPopup("Warning","Not enough automata to " + operationVerb + ".",
					"You must have at least one automata open." );
			return null;
		}
		
		// Creates a dialog to select two tabs
		ChoiceBox<String> dialog1 = new ChoiceBox<String>(tabNames);

		dialog1.setPrefWidth(200);
		dialog1.setValue(tabNames.get(0));
		
		GridPane gridPane = new GridPane();
		gridPane.setHgap(10);
		gridPane.setVgap(5);
		gridPane.add(dialog1, 1, 1);

		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle(operation);
		alert.setHeaderText("Pick an automata.");
		alert.getDialogPane().setContent(gridPane);
		
		// Get the select tabs if OK is selected and return the two
		// automatas in the tabs
		alert.showAndWait();
		if (alert.getResult() == ButtonType.OK){
			int index = tabNames.indexOf((dialog1.getValue()));
			automata = tabList.get(index);
			return automata;
		} else {
			return null;
		}
		
	}
	
	
	
	// Event Listener on MenuItem[#checkDeterminism].onAction
	/**
	 * Prompts the user to select a tab and displays a popup
	 * indicating whether or not the selected tab contains a
	 * deterministic finite automata.
	 */
	@FXML public void checkDeterminism(ActionEvent event) {
		// Have the user pick one automata
		Tab choice = pickOne("Determinism","check determinism");
		if (choice == null)
			return;
		
		boolean deterministic = AutomataOperations.isDeterministic(
				tabs.get(choice).getAutomata());
		String s = (deterministic) ? " is deterministic." 
				: " is not deterministic.";
		showPopup("Check Determinism", tabNameWithoutAsterisk(choice) + s, "");
	}
	
	// Event Listener on MenuItem[#checkEmptiness].onAction
	/**
	 * Prompts the user to select a tab and displays a popup
	 * indicating whether or not the selected tab contains an
	 * automata which accepts the empty language
	 */
	@FXML public void checkEmptiness(ActionEvent event) {
		// Have the user pick one automata
		Tab choice = pickOne("Emptiness","check emptiness");
		if (choice == null)
			return;
		
		boolean empty = AutomataOperations.acceptsEmptyLanguage(
				tabs.get(choice).getAutomata());
		String s = (empty) ? " accepts the empty language." 
				: " does not accept the empty language.";
		showPopup("Check Emptiness", tabNameWithoutAsterisk(choice) + s, "");
	}
	
	
	// Event Listener on MenuItem[#checkEquivalence].onAction
	/**
	 * Prompts the user to select two different automata tabs
	 * and displays a popup indicating whether or not the two
	 * chosen automata accept the same language.
	 */
	@FXML public void checkEquivalence(ActionEvent event) {
		
		// Have the user pick two automata
		Tab[] twoChoices = pickTwo("Equivalence","check equivalence");
		if (twoChoices == null)
			return;
		
		AutomataControl first = tabs.get(twoChoices[0]);
		AutomataControl second = tabs.get(twoChoices[1]);
		boolean equivalent = 
				AutomataOperations.checkEquivalence(first.getAutomata(),second.getAutomata());
		
		String s = (equivalent) ? " are equivalent." : " are not equivalent.";
		showPopup("Check Equivalence",
				tabNameWithoutAsterisk(twoChoices[0]) + " and " 
				+ tabNameWithoutAsterisk(twoChoices[1]) + s, "");
		
	}
	
	
	// Event Listener on MenuItem[#minimizeDFA].onAction
	/**
	 * Not implemented.  Prompts the user to select the tab of
	 * a deterministic automata and creates a new tab with its
	 * minimization.  Displays a popup warning if the selected tab
	 * contains a non-deterministic automata.
	 */
	@FXML public void minimizeDFA(ActionEvent event) {
		// TODO Autogenerated
	}
	/**
	 * Unimplemented. Prompts the user to select the tab of a non-deterministic
	 * automata and creates a new tab for a deterministic automata 
	 * which accepts the same language.  Displays a popup warning
	 * if the selected tab contains and already deterministic automata.
	 * deterministic finite automata.
	 */
	// Event Listener on MenuItem[#convertToDFA].onAction
	@FXML public void convertToDFA(ActionEvent event) {
		// TODO Autogenerated
	}
	
	
	// Event Listener on MenuItem[#union].onAction
	/**
	 * Prompts the user to select two different tabs and creates a
	 * new tab for an automata recognizing the union of the languages
	 * of the automata in the selected tabs.
	 */
	@FXML public void union(ActionEvent event) {

		// Have the user pick two automata
		Tab[] twoChoices = pickTwo("Union","union");
		if (twoChoices == null)
			return;
	
		AutomataControl first = tabs.get(twoChoices[0]);
		AutomataControl second = tabs.get(twoChoices[1]);
		AutomataCorrespondence correspondence = 
				AutomataOperations.unionWithCorrespondence(first.getAutomata(),second.getAutomata());
		
        // Create a new automata tab for the union
		AutomataControl union = new AutomataControl(correspondence.target);
		newAutomataTab(union, "Union of " 
				+ tabNameWithoutAsterisk(twoChoices[0])
				+ " and " 
				+ tabNameWithoutAsterisk(twoChoices[1])
				+ "*");
		
		
		// position the states and transitions corresponding to the first automata
		// with a 150 offset in the x direction
		Point2D.Double maxPos = position(first,union,correspondence.stateMap,150,0);
		// position the states and transitions corresponding to the second automata
		// with a 150x offset and 150y offset below the max Y of the first positions
		position(second,union,correspondence.stateMap,150,maxPos.getY()+150);
	
		// Position the new start state
		State unionStart = union.getAutomata().getStart();
		StateControl startControl = union.getStateMap().get(unionStart);
		startControl.setX(75);
		startControl.setY(maxPos.getY()+75);
		startControl.updateVisuals();
		
		// Reset the anchor position of the new states
		for (State s : correspondence.otherStates){
			for (Transition t : s.getTransitions()){
				union.getTransitionMap().get(t).resetAnchorPosition();
			}
		}
			
	}
	
	
	// Event Listener on MenuItem[#intersect].onAction
	/**
	 * Prompts the user to select two different tabs and creates a
	 * new tab for an automata recognizing the intersection of the languages
	 * of the automata in the selected tabs.
	 */
	@FXML public void intersect(ActionEvent event) {
		
		// Have the user pick two automata to intersect
		Tab[] twoChoices = pickTwo("Intersect","intersect");
		if (twoChoices == null)
			return;
	
		AutomataControl first = tabs.get(twoChoices[0]);
		AutomataControl second = tabs.get(twoChoices[1]);
		AutomataCorrespondence correspondence = 
				AutomataOperations.unionWithCorrespondence(first.getAutomata(),second.getAutomata());
		
        // Create a new automata tab for the intersection
		AutomataControl intersection = new AutomataControl(correspondence.target);
		newAutomataTab(intersection, "Intersection of " 
				+ tabNameWithoutAsterisk(twoChoices[0])
				+ " and " 
				+ tabNameWithoutAsterisk(twoChoices[1])
				+ "*");
		
		// Update the start state visual
		if (intersection.getStartState() != null)
			intersection.getStartState().updateVisuals();
	}
	
	
	
	// Event Listener on MenuItem[#complement].onAction
	/**
	 * Prompts the user to select a tab and creates a new tab
	 * with an automata recognizing the complement of the 
	 * automata on the selected tab.
	 */
	@FXML public void complement(ActionEvent event) {
		
		// Have the user pick one automata
		Tab choice = pickOne("Complement","complement");
		if (choice == null)
			return;
	
		AutomataControl automata = tabs.get(choice);
		AutomataCorrespondence correspondence = 
				AutomataOperations.complementWithCorrespondence(automata.getAutomata());
		
        // Create a new automata tab for the complement
		AutomataControl complement = new AutomataControl(correspondence.target);
		newAutomataTab(complement, "Complement of " 
				+ tabNameWithoutAsterisk(choice)
				+ "*");
		
		
		// position the states and transitions corresponding to the first automata
		Point2D.Double maxPos = position(automata,complement,correspondence.stateMap,0,0);
	
		// Position the new sink state
		State sink = correspondence.otherStates.get(0);
		StateControl sinkControl = complement.getStateMap().get(sink);
		sinkControl.setX(maxPos.getX()*0.5);
		sinkControl.setY(maxPos.getY()+100);
		
		// Reset the anchor position of the new states
		for (State s : correspondence.otherStates){
			for (Transition t : s.getTransitions()){
				complement.getTransitionMap().get(t).resetAnchorPosition();
			}
		}
		
		// Update the start state visual
		if (complement.getStartState() != null)
			complement.getStartState().updateVisuals();
	}
	
	
	
	// Event Listener on MenuItem[#concatenation].onAction
	/**
	 * Prompts the user to select two different tabs and creates a
	 * new tab for an automata recognizing the concatenation of the languages
	 * of the automata in the selected tabs.
	 */
	@FXML public void concatenation(ActionEvent event) {

		// Have the user pick two automata
		Tab[] twoChoices = pickTwo("Concantenation","concatenation");
		if (twoChoices == null)
			return;
	
		AutomataControl first = tabs.get(twoChoices[0]);
		AutomataControl second = tabs.get(twoChoices[1]);
		AutomataCorrespondence correspondence = 
				AutomataOperations.concatenationWithCorrespondence(first.getAutomata(),second.getAutomata());
		
        // Create a new automata tab for the concatenation
		AutomataControl concatenation = new AutomataControl(correspondence.target);
		newAutomataTab(concatenation, "Concatenation of " 
				+ tabNameWithoutAsterisk(twoChoices[0])
				+ " and " 
				+ tabNameWithoutAsterisk(twoChoices[1])
				+ "*");
		
		
		// position the states and transitions corresponding to the first automata
		Point2D.Double maxPos = position(first,concatenation,correspondence.stateMap,0,0);
		// position the states and transitions corresponding to the second automata
		// with a 150x offset and 150y offset below the max Y of the first positions
		position(second,concatenation,correspondence.stateMap,maxPos.getX()+150,0);
	
		// Reset the anchor position of the new states
		for (State s : correspondence.otherStates){
			for (Transition t : s.getTransitions()){
				concatenation.getTransitionMap().get(t).resetAnchorPosition();
			}
		}
		
		if (concatenation.getStartState() != null)
			concatenation.getStartState().updateVisuals();
		
	}
	
	
	// Event Listener on MenuItem[#kleeneStar].onAction
	/**
	 * Prompts the user to select a tab and creates a new tab
	 * with an automata recognizing the Kleene star of the 
	 * automata on the selected tab.
	 */
	@FXML public void kleeneStar(ActionEvent event) {
		
		// Have the user pick one automata
		Tab choice = pickOne("Kleene Star","Kleene star");
		if (choice == null)
			return;
	
		AutomataControl automata = tabs.get(choice);
		AutomataCorrespondence correspondence = 
				AutomataOperations.starWithCorrespondence(automata.getAutomata());
		
        // Create a new automata tab for the complement
		AutomataControl star = new AutomataControl(correspondence.target);
		newAutomataTab(star, "Kleene Star of " 
				+ tabNameWithoutAsterisk(choice)
				+ "*");
		
		
		// position the states and transitions corresponding to the first automata
		position(automata,star,correspondence.stateMap,150,0);
	
		// Position the new sink state
		State start = correspondence.otherStates.get(0);
		StateControl startControl = star.getStateMap().get(start);
		startControl.setX(75);
		startControl.setY(150);
		
		// Reset the anchor position of the new states
		for (State s : correspondence.otherStates){
			for (Transition t : s.getTransitions()){
				star.getTransitionMap().get(t).resetAnchorPosition();
			}
		}
		
		// Update the start state visual
		if (star.getStartState() != null)
			star.getStartState().updateVisuals();
		
	}
	

	/**
	 * Copies state and transition positions from the source AutomataControl onto the 
	 * target AutomataControl using the provided mapping and adjusted by the 
	 * specified offset.  This returns a point containing the maximum x and y
	 * coordinate reached by the source automata
	 * 
	 * @param source		the automata from which the positions should be taken
	 * @param target		the automata which should have its positions updated
	 * @param mapping		the mapping which dictates which positions correspond to
	 * 						which states
	 * @param xOffset		an offset applied to the x-coordinate of every state in 
	 * 						target automata
	 * @param yOffset		an offset applied to the y-coordinate of every state in 
	 * 						target automata
	 * @return				the maximum coordinates of the source automata
	 */
	private Point2D.Double position(AutomataControl source, AutomataControl target,
			Map<State,State> mapping, double xOffset, double yOffset){
		
		// Record the maximum y coordinate
		double maxY = 0;
		double maxX = 0;
		
		// State maps of the source and the target
		Map<State,StateControl> sourceSMap = source.getStateMap();
		Map<State,StateControl> targetSMap = target.getStateMap();
		
		// Copy the state positions from source to target with the
		// specified offset
		for (State s : source.getAutomata().getStates()){
			StateControl oldControl = sourceSMap.get(s);
			StateControl unionState = targetSMap.get(mapping.get(s));
			
			unionState.setX(oldControl.getX()+xOffset);
			unionState.setY(oldControl.getY()+yOffset);
			
			if (maxY < oldControl.getY())
				maxY = oldControl.getY();
			if (maxX < oldControl.getX())
				maxX = oldControl.getX();
		}
		
		// Transition maps of the source and target
		Map<Transition,TransitionControl> sourceTMap = source.getTransitionMap();
		Map<Transition,TransitionControl> targetTMap= target.getTransitionMap();
		
		// Copy the transition anchor positions from source to target with the
		// specified offset
		for (Transition t : source.getAutomata().getTransitions()){
			TransitionControl oldControl = sourceTMap.get(t);
			State sourceState = mapping.get(t.getSource());
			State targetState = mapping.get(t.getTarget());
			Transition unionT = target.getAutomata()
					.getTransition(sourceState, targetState);
			TransitionControl unionTransition = targetTMap.get(unionT);
			
			unionTransition.setAnchorX(oldControl.getAnchorX()+xOffset);
			unionTransition.setAnchorY(oldControl.getAnchorY()+yOffset);
			
			if (maxY < oldControl.getAnchorY())
				maxY = oldControl.getAnchorY();
			if (maxX < oldControl.getAnchorY())
				maxX = oldControl.getAnchorY();
		}
		
		// Return the maxY found
		return new Point2D.Double(maxX,maxY);
		
	}
	
	/**
	 * Returns the name of the tab without the asterisk which
	 * indicates that it is unsaved.
	 * @param t		the tab to get the name of
	 * @return		the name of the tab without a terminal asterisk
	 */
	private String tabNameWithoutAsterisk(Tab t){
		String tabName = t.getText();
		if (tabName.endsWith("*"))
			tabName = tabName.substring(0,tabName.length()-1);
		return tabName;
	}
	
	
	
}
