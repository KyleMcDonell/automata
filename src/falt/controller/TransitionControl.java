package falt.controller;

import java.util.HashSet;
import java.util.Set;

import falt.model.Matrix;
import falt.model.Transition;
import javafx.application.Platform;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Arc;
import javafx.scene.shape.ArcType;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.text.TextAlignment;

import java.awt.geom.Point2D;
import java.io.IOException;
import java.io.Serializable;


/**
 * A controller for the visual components of transitions in the view, responsible 
 * for changing the characters of a transition and calculating the arc it should
 * display as.
 * 
 * @author ncameron
 * @author kmcdonell
 */
public class TransitionControl implements Serializable {


	private static final long serialVersionUID = -7335238664452830489L;


	/** Holds the transition that this class controls.	 */
	private final Transition t;
	/** Holds the automata controller. */
	private final AutomataControl aut;
	/** Holds the source state controller. */
	private final StateControl source;
	/** Holds the target state controller. */
	private final StateControl target;


	/** Holds the arc that this class will calculate and display.	 */
	private transient Arc arc;
	/** Holds the anchor for defining the arc of the transition.	 */
	private transient Anchor.AnchorCircle anchor;
    
	/** The left line at the end of the transition */
    private transient Line arrowLeftLine;
    /** The right line at the end of the transition */
    private transient Line arrowRightLine;
	
	/** A label displaying the characters of a transition.	 */
	private transient Label labelDisplay;
	/** A warning label presented when the user gives a bad transition input character.	 */
	private transient Label inputWarning;
	/**
	 * The field the user interacts with, displaying the same text as the labelDisplay, 
	 * the characters of a transition.
	 */
	private transient TextField editableText;
	
	
	
	
	/**
	 * Used to calculate the arc and the arrowhead.
	 */
	private transient DoubleBinding minor11, xCenter, yCenter, radius, 
				angle1, angle2, angle3, startAngle, endAngle, arcLength,
				labelX, labelY, labelRotate, 
				intersectAngle, arrowX, arrowY, 
				arrowLeftX, arrowLeftY, arrowRightX, arrowRightY;
	
	/** Indicates which side of the target node the arrow should be placed */
	private transient BooleanBinding reverseArrowhead;
	
	
	/**
	 * Constructs a transition wrapper on a given transition from a source to a target.
	 * 
	 * @param t			the transition that we are wrapping
	 * @param source	the source wrapped state of the transition
	 * @param target	the target wrapped state of the transition
	 * @param aut		the AutomataControl handles operations like string testing
	 */
	public TransitionControl(Transition t, StateControl source, StateControl target, 
			AutomataControl aut){
		
		this.t = t;
		this.aut = aut;
		this.source = source;
		this.target = target;
		
		createVisuals();
		setUpBindings();
		enableInteraction();
		
		// If the transition is empty, add an epslion to the transition
		// and start editing
		if (t.getCharacters().isEmpty()){
			t.addCharacter(null);
			editableText.setVisible(true);
			labelDisplay.setVisible(false);
			Platform.runLater( ()-> editableText.requestFocus());
			
		// Otherwise update the label to reflect the transition
		} else {
			String transitionString = interpretTransition();
			editableText.setText(transitionString);
			labelDisplay.setText(transitionString);
		}
		
	}
	
	/**
	 * Returns the transition that the controller is responsible for.
	 * 
	 * @return		the Transition that the controller is responsible for
	 */
	public Transition getTransition(){ return t; }
	
	/**
	 * Returns an observable list of the visual elements held in the 
	 * transition controller.
	 * 
	 * @return	an ObservableList<Node> of visual elements
	 */
	public ObservableList<Node> getVisuals(){ 
		return FXCollections.observableArrayList(
				arc, anchor, labelDisplay, inputWarning, editableText, 
				arrowLeftLine, arrowRightLine);
	}
	
	/**
	 * Returns (x, y) position of the anchor used to shape the transition.
	 * 
	 * @return	a Point2D.Double holding the position of the anchor
	 */
	public Point2D.Double getAnchorPosition(){
		return new Point2D.Double(anchor.getCenterX(),anchor.getCenterY());
	}
	
	/**
	 * Sets the transition's anchor position to the provided point
	 * @param point		the point the anchor should be moved to
	 */
	public void setAnchorPosition(Point2D.Double point){
		setAnchorX(point.getX());
		setAnchorY(point.getY());
	}
	
	/** 
	 * Gets the x-coordinate of the transition's anchor point
	 * @return 		the anchor x-coordinate
	 */
	public double getAnchorX(){ return anchor.getCenterX(); }
	
	/** 
	 * Gets the y-coordinate of the transition's anchor point
	 * @return 		the anchor y-coordinate
	 */
	public double getAnchorY(){ return anchor.getCenterY(); }
	
	/**
	 * Sets the x-coordinate of the transition's anchor point
	 * @param x		the new anchor x-coordinate
	 */
	public void setAnchorX(double x){ anchor.setCenterX(x); }
	
	/**
	 * Sets the y-coordinate of the transition's anchor point
	 * @param y		the new anchor y-coordinate
	 */
	public void setAnchorY(double y){ anchor.setCenterY(y); }

	/**
	 * Resets the position of the transition's anchor point to be
	 * a distance perpendicular to the midpoint between the source
	 * and target state positions.
	 */
	public void resetAnchorPosition(){
		if (source != target) {
			double midPointX = 0.5 * (source.getX() + target.getX());
			double midPointY = 0.5 * (source.getY() + target.getY());
			double dx, dy;
			
			// Move the anchor perpendicular to the line between the nodes.
			if (source.getX() != target.getX() && source.getY() != target.getY()){
				double slope = (target.getY()-source.getY())/(target.getX()-source.getX());
				double perpSlope = -1/slope;
				dx = 20*Math.cos(Math.atan(perpSlope));
				dy = 20*Math.sin(Math.atan(perpSlope));
			} else if (source.getX() == target.getX()) {
				dx = 4;
				dy = 0;
			} else {
				dx = 0;
				dy = 4;
			}
			
			double sign = (source.hashCode() > target.hashCode()) ? 1 : -1;
			anchor.setCenterX(midPointX+sign*dx);
			anchor.setCenterY(midPointY+sign*dy);
		} else {
			anchor.setCenterX(source.getX());
			anchor.setCenterY(source.getY()-50);
		}
	}
	
	/**
	 * Shows or hides the transition's anchor point
	 * @param visible		the new visibility of the anchor
	 */
	public void showAnchor(boolean visible){ anchor.setVisible(visible); }
	
	
	/**
	 * Initializes the visual elements held in the transition controller.
	 */
	private void createVisuals(){
	
		// Initializes the anchor
		if (source != target) {
			double midPointX = 0.5 * (source.getX() + target.getX());
			double midPointY = 0.5 * (source.getY() + target.getY());
			double dx, dy;
			
			// Move the anchor perpendicular to the line between the nodes.
			if (source.getX() != target.getX() && source.getY() != target.getY()){
				double slope = (target.getY()-source.getY())/(target.getX()-source.getX());
				double perpSlope = -1/slope;
				dx = 20*Math.cos(Math.atan(perpSlope));
				dy = 20*Math.sin(Math.atan(perpSlope));
			} else if (source.getX() == target.getX()) {
				dx = 4;
				dy = 0;
			} else {
				dx = 0;
				dy = 4;
			}
			
			double sign = (source.hashCode() > target.hashCode()) ? 1 : -1;
			Anchor a = new Anchor(Color.BLACK, midPointX+sign*dx, 
					midPointY+sign*dy, aut.getUndoManager());
			anchor = a.getVisual();
		
		// If this is a self loop, place the anchor slightly above the state
		} else {
			Anchor a = new Anchor(Color.BLACK, source.getX(), source.getY()-50,
					aut.getUndoManager());
			anchor = a.getVisual();
		}
		
		
		anchor.setVisible(false);
		
		// Initializes the arc
		arc = new Arc();
		arc.setStroke(Color.BLACK);
		arc.setStrokeWidth(3);
		arc.setStrokeLineCap(StrokeLineCap.ROUND);
		arc.setFill(null);
		arc.setType(ArcType.OPEN);
		
		// Initializes the arrowhead
		arrowLeftLine = new Line();
		arrowLeftLine.setStroke(Color.BLACK);
		arrowLeftLine.setStrokeWidth(3);
		arrowLeftLine.setVisible(true);
		arrowRightLine = new Line();
		arrowRightLine.setStroke(Color.BLACK);
		arrowRightLine.setStrokeWidth(3);
		arrowRightLine.setVisible(true);
		
		// Initializes the labels
		String s = interpretTransition();
		labelDisplay = new Label(s);
		labelDisplay.textAlignmentProperty().set(TextAlignment.CENTER);
		editableText = new TextField(s);
		editableText.setVisible(false);
		inputWarning = new Label("No more than 1 char between each comma!");
		inputWarning.setTextFill(Paint.valueOf("red"));
		inputWarning.setVisible(false);
		
		
	}
	
	
	/**
	 * Properly binds all relevant information to handle drawing of the arc, the
	 * transition label, and the arrowhead.
	 * 
	 * @param source	the source StateWrapper of the transition
	 * @param target	the target StateWrapper of the transition
	 */
	private void setUpBindings(){
		
		Circle sourceCircle = source.getStateVisual();
		Circle targetCircle = target.getStateVisual();
		
		DoubleProperty x1 = sourceCircle.centerXProperty();
		DoubleProperty y1 = sourceCircle.centerYProperty();
		DoubleProperty x2 = anchor.centerXProperty();
		DoubleProperty y2 = anchor.centerYProperty();
		DoubleProperty x3 = targetCircle.centerXProperty();
		DoubleProperty y3 = targetCircle.centerYProperty();
		
		createArcBindings(x1,y1,x2,y2,x3,y3);
		createArrowBindings(x1,y1,x2,y2,x3,y3);
		createLabelBindings(x2,y2);
		
		// Bind all of the properties
		arc.centerXProperty().bind(xCenter);
		arc.centerYProperty().bind(yCenter);
		arc.radiusXProperty().bind(radius);
		arc.radiusYProperty().bind(radius);
		arc.startAngleProperty().bind(startAngle);
		arc.lengthProperty().bind(arcLength);
		
		arrowLeftLine.startXProperty().bind(arrowX);
		arrowLeftLine.startYProperty().bind(arrowY);
		arrowLeftLine.endXProperty().bind(arrowLeftX);
		arrowLeftLine.endYProperty().bind(arrowLeftY);
		
		arrowRightLine.startXProperty().bind(arrowX);
		arrowRightLine.startYProperty().bind(arrowY);
		arrowRightLine.endXProperty().bind(arrowRightX);
		arrowRightLine.endYProperty().bind(arrowRightY);
		
		labelDisplay.rotateProperty().bind(labelRotate);
		labelDisplay.layoutXProperty().bind(labelX);
		labelDisplay.layoutYProperty().bind(labelY);
		editableText.layoutXProperty().bind(labelX);
		editableText.layoutYProperty().bind(labelY);
		inputWarning.layoutXProperty().bind(labelX);
		inputWarning.layoutYProperty().bind(labelY.subtract(20));
		
	}
	
	/**
	 * Creates the bindings used to manage the transition arc
	 * 
	 * @param x1	the source state's x-coordinate
	 * @param y1	the source state's y-coordinate
	 * @param x2	the anchor's x-coordinate
	 * @param y2	the anchor's y-coordinate
	 * @param x3	the targets state's x-coordinate
	 * @param y3	the targets state's y-coordinate
	 */
	private void createArcBindings(DoubleProperty x1, DoubleProperty y1, DoubleProperty x2,
			DoubleProperty y2, DoubleProperty x3, DoubleProperty y3){
		
		/*
		 Minors are from the matrix :
		 
		{	x0^2+y0^2,	 x0,	 y0,	 1	}
		{	x1^2+y1^+2,	 x1,	 y1,	 1	}
		{	x2^2+y2^2,	 x2,	 y2,	 1	}
		{	x3^2+y3^2,	 x3,	 y3,	 1	}
		
		 (x0,y0) is the center of the circle passing through the source: (x1,y1), 
		 the anchor: (x2,y2), and the target: (x3,y3).
		 
		 This can be used to solve for x0 and y0, i.e. to find the center of a cirlce
		 passing through the three given points
		 
		*/
		
		
		// Used to calculate xCenter and yCenter
		minor11 = new DoubleBinding(){
			{   super.bind(x1,y1,x2,y2,x3,y3);  }
			@Override public double computeValue(){
				double[][] matrix = {{x1.get(), y1.get(), 1},
									 {x2.get(), y2.get(), 1},
									 {x3.get(), y3.get(), 1}};
				double minor11 = Matrix.determinant(matrix);
				
				// If the points are colinear, slightly shift the anchor
				if (Double.compare(minor11,0) == 0){
					double epsilon = 0.01;
					anchor.setCenterX(x2.get()+epsilon);
					double[][] matrix2 = {{x1.get(), y1.get(), 1},
										 {x2.get(), y2.get(), 1},
										 {x3.get()-epsilon, y3.get()-epsilon, 1}};
					minor11 = Matrix.determinant(matrix2);
				}
				
				return minor11;
			}
		};
		
		// The x-coord of the center of the circle
		xCenter = new DoubleBinding(){
			{	super.bind(x1,y1,x2,y2,x3,y3,minor11);	}
			@Override public double computeValue(){
				
				// If this is a self-transition, simply take the midpoint between this and the anchor
				if (source == target)
					return 0.5 * (x1.get() + x2.get());
				
				double[][] matrix = {{Math.pow(x1.get(),2)+Math.pow(y1.get(),2), y1.get(), 1},
						 			 {Math.pow(x2.get(),2)+Math.pow(y2.get(),2), y2.get(), 1},
						 			 {Math.pow(x3.get(),2)+Math.pow(y3.get(),2), y3.get(), 1}};
				double minor12 = Matrix.determinant(matrix);
				
				return .5*minor12/minor11.get();
			}
		};
		
		// The y-coord of the center of the circle
		yCenter = new DoubleBinding(){
			{	super.bind(x1,y1,x2,y2,x3,y3,minor11);	}
			@Override public double computeValue(){
				
				// If this is a self-transition, simply take the midpoint between this and the anchor
				if (source == target)
					return 0.5 * (y1.get() + y2.get());
				double[][] matrix = {{Math.pow(x1.get(),2)+Math.pow(y1.get(),2), x1.get(), 1},
			 			 			 {Math.pow(x2.get(),2)+Math.pow(y2.get(),2), x2.get(), 1},
			 			 			 {Math.pow(x3.get(),2)+Math.pow(y3.get(),2), x3.get(), 1}};
				double minor13 = Matrix.determinant(matrix);
				return -.5*minor13/minor11.get();
			}
		};
		
		// The distance between the center and the source: (x1,y1)
		radius = new DoubleBinding(){
			{	super.bind(x1,y1,xCenter,yCenter);	}
			@Override public double computeValue(){
				return Math.sqrt( Math.pow(xCenter.get()-x1.get(), 2) 
						+ Math.pow(yCenter.get()-y1.get(), 2));
			}
		};
		
		// The angle in degrees from the center to the source (x1,y1)
		angle1 = new DoubleBinding(){
			{	super.bind(x1,y1,xCenter,yCenter);	}
			@Override public double computeValue(){
				double angle1 = (Math.toDegrees(
						Math.atan2(-y1.get()+yCenter.get(), x1.get()-xCenter.get() ) ) + 360) % 360;
				return angle1;
				
			}
		};
		
		// The angle in degrees from the center to the anchor (x2,y2)
		angle2 = new DoubleBinding(){
			{	super.bind(x2,y2,xCenter,yCenter);	}
			@Override public double computeValue(){
				double angle2 = (Math.toDegrees(
						Math.atan2(-y2.get()+yCenter.get(), x2.get()-xCenter.get() ) ) + 360) % 360;
				return angle2;
				
			}
		};

		// The angle in degrees from the center to the target (x3,y3)
		angle3 = new DoubleBinding(){
			{	super.bind(x3,y3,xCenter,yCenter);	}
			@Override public double computeValue(){
				double angle3 = (Math.toDegrees(
						Math.atan2(-y3.get()+yCenter.get(), x3.get()-xCenter.get() ) ) + 360) % 360;
				return angle3;
				
			}
		};

		// The smaller of the two angles
		startAngle = new DoubleBinding(){
			{	super.bind(angle1, angle3);	}
			@Override public double computeValue(){
				return Math.min(angle1.get(), angle3.get());
				
			}
		};
		
		// The larger of the two angles
		endAngle = new DoubleBinding(){
			{	super.bind(angle1, angle3);	}
			@Override public double computeValue(){
				return Math.max(angle1.get(), angle3.get());
				
			}
		};
		
		//The arc length in degrees of the transition
		arcLength = new DoubleBinding(){
			{	super.bind(startAngle,angle2,endAngle);	}
			@Override public double computeValue(){

				double arcLength;
				// If the anchor is between the start and end angle, the arcLength should be positive
				// and go from the start to the end
				if (angle2.get() > startAngle.get() && angle2.get() < endAngle.get())
					arcLength = endAngle.get() - startAngle.get();
				// Otherwise it should be negative and go backwards
				else 
					arcLength = endAngle.get() - startAngle.get() - 360;
				
				// If the length is 0, make it 360 instead
				return (arcLength != 0) ? arcLength : 360;
			}
		};	
		
	}
	
	/**
	 * Creates the bindings used to manage the transition arrow
	 * 
	 * @param x1	the source state's x-coordinate
	 * @param y1	the source state's y-coordinate
	 * @param x2	the anchor's x-coordinate
	 * @param y2	the anchor's y-coordinate
	 * @param x3	the targets state's x-coordinate
	 * @param y3	the targets state's y-coordinate
	 */
	private void createArrowBindings(DoubleProperty x1, DoubleProperty y1, DoubleProperty x2,
			DoubleProperty y2, DoubleProperty x3, DoubleProperty y3){
		
		// Whether or not the arrow should be reversed, determined by whether or not
		// the three points in order (x1,y1), (x2,y2), (x3,y3) appear in cw or ccw
		reverseArrowhead = new BooleanBinding(){
			{	super.bind(x1,y1,x2,y2,x3,y3);	}
			@Override public boolean computeValue(){
				double[][] matrix = {{1, x1.get(), y1.get()},
									 {1, x2.get(), y2.get()},
									 {1, x3.get(), y3.get()}};
				double det = Matrix.determinant(matrix);
				return det < 0;
			}
		};
		
		// Angle of the point at which the arc intersects the target state IN RADIANS
		intersectAngle = new DoubleBinding(){
			{	super.bind(angle3,radius,reverseArrowhead);	}
			@Override public double computeValue(){
				// Law of cosines is used to find the angle of the arc between the intersection and
				// the center of the circle
				double angle = Math.acos(
						( Math.pow(radius.get(), 2) +  Math.pow(radius.get(), 2) 
						- Math.pow(target.getStateVisual().getRadius(), 2) ) 
						/ (2*Math.pow(radius.get(),2)) 
						);
				double signedArcAngle = (reverseArrowhead.get()) ? -angle : angle;
				double intersectAngle = Math.toRadians(angle3.get())
						+ signedArcAngle;
				return intersectAngle;
			}
		};
		
		// arrowX and arrowY form the intersection point, where the arrowhead should be placed
		// This is the point the transition meets (x3,y3)
		arrowX = new DoubleBinding(){
			{	super.bind(xCenter,radius,intersectAngle);	}
			@Override public double computeValue(){
				double x = xCenter.get() + radius.get()*Math.cos(intersectAngle.get());
				return x;
			}
		};
		arrowY = new DoubleBinding(){
			{	super.bind(yCenter,radius,angle3,reverseArrowhead);	}
			@Override public double computeValue(){
				double y = yCenter.get() - radius.get()*Math.sin(intersectAngle.get());
				return y;
			}
		};
		
		
		// one of the lines goes to (arrowLeftX,arrowLeftY)
		arrowLeftX = new DoubleBinding(){
			{	super.bind(arrowX,intersectAngle,reverseArrowhead);	}
			@Override public double computeValue(){
				final double length = 12;
				// The angle the arrow should be offset from the line tanget to the arc
				double offset = (reverseArrowhead.get()) ? Math.PI/4.0 : Math.PI+Math.PI/4.0;
				// The angle the arrow should come off the intersection
				double arrowAngle = intersectAngle.get() - 0.5*Math.PI + offset;
				return arrowX.get() + length * Math.cos(arrowAngle);
				
			}
		};
		arrowLeftY = new DoubleBinding(){
			{	super.bind(arrowY,intersectAngle,reverseArrowhead);	}
			@Override public double computeValue(){
				final double length = 12;
				// The angle the arrow should be offset from the line tanget to the arc
				double offset = (reverseArrowhead.get()) ? Math.PI/4.0 : Math.PI+Math.PI/4.0;
				// The angle the arrow should come off the intersection
				double arrowAngle = intersectAngle.get() - 0.5*Math.PI + offset;
				return arrowY.get() - length * Math.sin(arrowAngle);
				
			}
		};
		
		// and the other goes to (arrowRightX, arrowRightY)
		arrowRightX = new DoubleBinding(){
			{	super.bind(arrowX,reverseArrowhead);	}
			@Override public double computeValue(){
				final double length = 12;
				// The angle the arrow should be offset from the line tanget to the arc
				double offset = (reverseArrowhead.get()) ? Math.PI/4.0 : Math.PI+Math.PI/4.0;
				// The angle the arrow should come off the intersection
				double arrowAngle = intersectAngle.get() - 0.5*Math.PI - offset;
				return arrowX.get() + length * Math.cos(arrowAngle);
			}
		};
		arrowRightY = new DoubleBinding(){
			{	super.bind(arrowY,reverseArrowhead);	}
			@Override public double computeValue(){
				final double length = 12;
				// The angle the arrow should be offset from the line tanget to the arc
				double offset = (reverseArrowhead.get()) ? Math.PI/4.0 : Math.PI+Math.PI/4.0;
				// The angle the arrow should come off the intersection
				double arrowAngle = intersectAngle.get() - 0.5*Math.PI - offset;
				return arrowY.get() - length * Math.sin(arrowAngle);
			}
		};

	}
	
	/**
	 * Creates the bindings used to manage the label position
	 * 
	 * @param x2	the anchor's x-coordinate
	 * @param y2	the anchor's y-coordinate
	 */
	private void createLabelBindings(DoubleProperty x2, DoubleProperty y2){

		ReadOnlyDoubleProperty width = labelDisplay.widthProperty();
		ReadOnlyDoubleProperty height = labelDisplay.heightProperty();
		
		// The X-position of the center of the transition label
		labelX = new DoubleBinding(){
			{	super.bind(x2,angle2,width);		}
			@Override public double computeValue(){;
				double offset = Math.cos(Math.toRadians(angle2.get())) * 15;
				return x2.get() + offset - 0.5 * width.get();
			}
			
		};
		
		// The Y-position of the center of the transition label
		labelY = new DoubleBinding(){
			{	super.bind(y2,angle2,height);		}
			@Override public double computeValue(){
				double offset = Math.sin(Math.toRadians(angle2.get())) * 15;
				return y2.get() - offset - 0.5 * height.get();
			}
		};
		
		// The angle to rotate the transition label to align it with the anchor
		labelRotate = new DoubleBinding(){
			{	super.bind(angle2);		}
			@Override public double computeValue(){
				double textAngle = 90 - angle2.get();
				return (textAngle + 90 >= 0) ? textAngle : textAngle+180;
			}
		};
		
	}
	
	
	/**
	 * Adds event handlers for the arc, TextField label, and Label label.
	 */
	public void enableInteraction() {
		
		// changes anchor visibility and handles removal
		arc.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override public void handle(MouseEvent mouseEvent) {
				InteractState state = aut.getInteractState();
				if (state == InteractState.MOUSE || state == InteractState.CREATE
						|| state == InteractState.TESTING){
					anchor.setVisible(!anchor.isVisible());
				} else if (state == InteractState.DELETE){
					System.out.println("Deleting");
					aut.removeTransition(TransitionControl.this);
				}
				mouseEvent.consume();
			}
		});
		
		
		// on clicks, the label should become editable
		labelDisplay.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override public void handle(MouseEvent mouseEvent) {
				
				InteractState state = aut.getInteractState();
				if (state == InteractState.MOUSE || state == InteractState.CREATE){
						editableText.setVisible(true);
						labelDisplay.setVisible(false);
						Platform.runLater(() -> editableText.requestFocus());
					
				} else if (state == InteractState.DELETE){
						aut.removeTransition(TransitionControl.this);
				}
				mouseEvent.consume();
			}
		});
		
		// Allow the user to submit changes with enter
		editableText.setOnKeyPressed(new EventHandler<KeyEvent>(){
			@Override
			public void handle(KeyEvent keyEvent){
				if (keyEvent.getCode() == KeyCode.ENTER){
					editableText.getParent().requestFocus();
				}
			}
		});
		
		// the label TextField holds focus until it gets good input
		editableText.focusedProperty().addListener(new ChangeListener<Boolean>(){
			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, 
					Boolean oldValue, Boolean newValue){
				
				// If focus is lost, parse the text
				if (!newValue) {
					
					String input = editableText.getText();
					
					// If the input is bad, regain focus and display warning
					if (!isGoodTransitionInput(input)){
						editableText.requestFocus();
						inputWarning.setVisible(true);
					
					// Otherwise update the transition
					} else {
						
						// If the input is empty, replace it with epsilon which is automatically replaced with epsilon
						if (input.length() == 0){
							input = "\u025B";
						} else {
							// Get copies of the transitionChange
							Set<Character> oldChars = new HashSet<>(t.getCharacters());
							Set<Character> newChars = getTransitionChars(input);
							
							// If the transition was actually changed, update it and create a new action
							if (!oldChars.containsAll(newChars) || !newChars.containsAll(oldChars)){
								
								// Get the characters from the text and update the transition
								t.setCharacters(getTransitionChars(input));
								aut.getUndoManager().addAction(new TransitionTextChange(oldChars, newChars));
							}
						}
							
	
						// Update the label and editBox to reflect the transition and change whats displayed
						String updatedString = interpretTransition();
						editableText.setVisible(false);
						inputWarning.setVisible(false);
						labelDisplay.setVisible(true);
						
						editableText.setText(updatedString);
						labelDisplay.setText(updatedString);
						
					}
				}
			}
			
		});
		
		// Replace double commas and spaces with epsilon while the transition is being edited
		editableText.textProperty().addListener(new ChangeListener<String>(){
			@Override
			public void changed(ObservableValue<? extends String> arg0,
					String oldValue, String newValue){
				
				// Replace spaces with epsilon
				if (newValue.contains(" ")){
					editableText.setText(newValue.replace(" ", "\u025B"));
				}
				if (newValue.startsWith(",")){
					editableText.setText(newValue.replaceFirst(",","\u025B,"));
				}
				
				// Replace double commas with epsilon
				if (newValue.startsWith(",,", 0)){
					editableText.setText(newValue.replace(",", "\u025B,"));
				} else if (newValue.contains(",,")){
					editableText.setText(newValue.replace(",,", ",\u025B,"));
				}
			}
		});
		
		
		
	}
	
	/**
	 * Checks if input for transition characters is acceptable. A transition
	 * label must contain no more than 1 character between each comma.
	 * 
	 * @param input		the string to check for acceptability
	 * @return			true if the string is good, else false
	 */
	private boolean isGoodTransitionInput(String input){
		String[] inputs = input.split(",");
		for (String str : inputs){
			if (str.length() > 1){
				return false;
			}
		}
		return true;
	}
	
	
	/**
	 * Gets acceptable input for transition characters for putting into the 
	 * transition in the model.
	 * 
	 * @param input		the input to parse and add characters from to the transition
	 * @return			the set of characters that were added
	 */
	private Set<Character> getTransitionChars(String input){
		Set<Character> chars = new HashSet<Character>();
		String[] inputs = input.split(",");
		for (String str : inputs){
			if (str.equals("\u025B")){
				chars.add(null);
			} else if (str.length() == 1){
				chars.add(str.charAt(0));
			} else {
				throw new IllegalArgumentException();
			}
		}
		return chars;
	
	}
	
	/**
	 * Returns a string representation of the transition, to be displayed in the
	 * Label and editable TextField that the user sees and interacts with 
	 * respectively.
	 * 
	 * @return		a string representation of the transition
	 */
	private String interpretTransition(){
		// Build a string from the characters in the transition
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		for (Character ch : t.getCharacters()){
			if (first){
				first = false;
			} else {
				sb.append(',');
			}
			// If the transition includes null, add an epsilon symbol
			sb.append( (ch != null) ? ch : '\u025B');
		}
		return sb.toString();
	}

	
	
	
	
	/**
	 * Writes the TransitionControl object to an output stream.
	 * 
	 * @param out	the ObjectOutputStream to write to
	 * @throws IOException	
	 */
	private void writeObject(java.io.ObjectOutputStream out) throws IOException{
		out.defaultWriteObject();
		out.writeObject(getAnchorPosition());
	}
	
	/**
	 * Reads a TransitionControl object from an inputstream, and reconstructs
	 * non-serialized elements.
	 * 
	 * @param in	the ObjectInputStream to read from
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private void readObject(java.io.ObjectInputStream in)
			throws IOException, ClassNotFoundException{
		in.defaultReadObject();
		Point2D.Double p = (Point2D.Double) in.readObject();
		
		createVisuals();
		setUpBindings();
		enableInteraction();
		
		anchor.setCenterX(p.getX());
		anchor.setCenterY(p.getY());
		
		// If the transition is empty, and an epsilon
		if (t.getCharacters().isEmpty())
			t.addCharacter(null);
			
		// Update the label to reflect the transition
		String transitionString = interpretTransition();
		editableText.setText(transitionString);
		labelDisplay.setText(transitionString);
		
	}
	
	/**
	 * Action representing a change in transition text so the change
	 * can be undone and redone.
	 * 
	 * @author ncameron
	 * @author kmcdonel
	 */
	private class TransitionTextChange implements ActionPattern{
		
		private Set<Character> oldChars, newChars;
		
		public TransitionTextChange(Set<Character> oldChars, Set<Character> newChars){
			this.oldChars = oldChars;
			this.newChars = newChars;
		}
		
		@Override public void redo() {
			t.setCharacters(newChars);
			updateLabels();
			
		}
		@Override public void undo() {
			t.setCharacters(oldChars);
			updateLabels();
		}
		
		private void updateLabels(){
			String updatedString = TransitionControl.this.interpretTransition();
			editableText.setText(updatedString);
			labelDisplay.setText(updatedString);	
		}
		
	}
	
}
