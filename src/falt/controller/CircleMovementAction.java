package falt.controller;

import javafx.scene.shape.Circle;

/**
 * ActionPattern for the movement of a circle.  This allows such
 * actions to be undone.
 * 
 * @author ncameron
 * @author kmcdonell
 */
public class CircleMovementAction implements ActionPattern {

	/** The visual component of the circle movement action. */
	private Circle c;
	/** The changes in x coordinates of the circle. */
	private double dx;
	/** The changes in y coordinates of the circle. */
	private double dy;
	
	/**
	 * Initializes a circle movement action for the given circle and position
	 * changes. 
	 * 
	 * @param c		the circle that was moved
	 * @param dx	the change in x position
	 * @param dy	the change in y position
	 */
	public CircleMovementAction(Circle c, double dx, double dy){
		this.c = c;
		this.dx = dx;
		this.dy = dy;
	}
	
	/**
	 * Allows the action to be redone.
	 */
	@Override
	public void redo() {
		c.setCenterX(c.getCenterX()+dx);
		c.setCenterY(c.getCenterY()+dy);
	}
	
	/**
	 * Allows the action to be undone.
	 */
	@Override
	public void undo() {
		c.setCenterX(c.getCenterX()-dx);
		c.setCenterY(c.getCenterY()-dy);
	}

}
