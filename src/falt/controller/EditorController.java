package falt.controller;


import java.net.URL;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.Stack;

import falt.model.FiniteAutomata;
import falt.model.NoStartStateException;
import falt.model.State;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 * Class to control an automata editor tab.  It includes responsibility for string
 * testing and handling user interaction state.
 * 
 * @author ncameron
 * @author kmcdonell
 */

public class EditorController implements Initializable {
	
	/** The intersection state of the editor */
	InteractState interactState = InteractState.MOUSE;
	/** 
	 * A variable to store what interaction state the editor was in
	 * before testing began 
	 */
	InteractState oldInteractState;
	
	/** The automata control which this editor manages */
	private AutomataControl aut;
	
	// The main components of the editor injected by FXML
	@FXML private ScrollPane automataScrollPane;
	@FXML private ToolBar editorToolBar;
	
	
	// Buttons injected by the FXML file
	@FXML private ToggleButton mouseButton;
	@FXML private ToggleButton createButton;
	@FXML private ToggleButton deleteButton;
	@FXML private Button undoButton;
	@FXML private Button redoButton;
	
	// Step testing strings and buttons injected by FXML
	@FXML private VBox testingBox;
	@FXML private Label testingText;
	@FXML private TextField testStringBox;
	@FXML private Button testStringButton;
	@FXML private Button resetStringButton;
	@FXML private Label testStringLabel;
	@FXML private Button stepButton;
	@FXML private Button backStepButton;
	@FXML private Label testResult;
	
	// Batch testing objects injected by FXML
	@FXML private Button batchTestButton;
	@FXML private Button clearBatchButton;
	@FXML private ScrollPane batchTestScrollPane;
	@FXML private TableView<TableController.TestEntry> batchTestTable = new TableView<>();
	
	
	
	/** Holds the current index in the string that is being tested. */
	private IntegerProperty index = new SimpleIntegerProperty();
	/**
	 * Holds a stack of sets of states for the user to step back and
	 * forward through.
	 */
	private Stack<Set<State>> stateHistory = new Stack<>();
	
	
	
	
	/** Initializes the editor controller. */
	@Override
	public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
		
		
		// Initialize batch test table
		TableController.initializeBatchTable(batchTestTable);
		
		// Hide the test materials
		showTestMaterial(false);
		testResult.setVisible(false);
		
		
		// Add a listener to replace spaces with epsilon in the step testing
		testStringBox.textProperty().addListener(new ChangeListener<String>(){
			@Override
			public void changed(ObservableValue<? extends String> arg0,
					String oldValue, String newValue){
				if (newValue.contains(" ")){
					testStringBox.setText(newValue.replace(" ", "\u025B"));
				}
			}
		});
		
		// Add the editor buttons to a group
		final ToggleGroup group = new ToggleGroup();
		mouseButton.setToggleGroup(group);
		createButton.setToggleGroup(group);
		deleteButton.setToggleGroup(group);
		mouseButton.setSelected(true);
		
		// Ensures a mode button is always selected
		group.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
			@Override
            public void changed(ObservableValue<? extends Toggle> ov,
                    Toggle toggle, Toggle new_toggle) {
                if (new_toggle == null) {
                    toggle.setSelected(true);
                }
            }
        });
		
	}
	
	
	/**
	 * Link the undo and redo buttons to the undoManager so they are disabled
	 * if there is nothing to undo or redo
	 */
	private void linkUndoButtons(){
		
		UndoManager undoManager = aut.getUndoManager();
		undoButton.setDisable(!undoManager.getCanUndo());
		redoButton.setDisable(!undoManager.getCanRedo());
		
		// Add a listener to the undoManagers properties
		undoManager.canUndoProperty().addListener(new ChangeListener<Boolean>() {
			@Override
            public void changed(ObservableValue<? extends Boolean> ov,
            		Boolean oldValue, Boolean newValue) {
                undoButton.setDisable(!newValue);
			}
        });
		undoManager.canRedoProperty().addListener(new ChangeListener<Boolean>() {
			@Override
            public void changed(ObservableValue<? extends Boolean> ov,
            		Boolean oldValue, Boolean newValue) {
                redoButton.setDisable(!newValue);
			}
        });
	}
	
	
	
	/**
	 * Sets the automata reference to the given value.
	 * @param automata	an AutomataControl instance
	 */
	public void setAutomata(AutomataControl automata){	
		aut = automata;	
		linkUndoButtons();
	}
	
	/**
	 * Returns the automata controller.
	 * @return	the AutomataControl instance
	 */
	public AutomataControl getAutomata(){  return aut; }
	
	/**
	 * Returns the scroll pane holding the automata.
	 * @return	a scroll pane holding an automata
	 */
	public ScrollPane getScrollPane(){	return automataScrollPane;	}
	
	/**
	 * Returns which interactive state the editor is in.
	 * @return	the current interactive state
	 */
	public InteractState getInteractState(){ return interactState; }
	
	
	
	
	
	
	// Methods to change the interactState and focus on the automata pane -----------------------------
	
	// Event Listener on Button[#mouseButton].onAction
	/** Set the editor to mouse mode */
	@FXML public void mouseMode(ActionEvent event) {
		interactState = InteractState.MOUSE;
		aut.getViewPane().requestFocus();
	}
	// Event Listener on Button[#createButton].onAction
	/** Set the editor to create mode */
	@FXML public void createMode(ActionEvent event) {
		interactState = InteractState.CREATE;
		aut.getViewPane().requestFocus();
	}
	// Event Listener on Button[#deleteButton].onAction
	/** Set the editor to delete mode */
	@FXML public void deleteMode(ActionEvent event) {
		interactState = InteractState.DELETE;
		aut.getViewPane().requestFocus();
	}
	
	
	
	// Undo and redo buttons ---------------------------------------------------------------------------
	
	
	// Event Listener on Button[#undoButton].onAction
	/** Undo the last action on the automata */
	@FXML public void undoAction(ActionEvent event) {
		aut.undoLastAction();
	}
	// Event Listener on Button[#redoButton].onAction
	/** Redo the last actio on the automata */
	@FXML public void redoAction(ActionEvent event) {
		aut.redoLastAction();
	}
	

	// Batch testing interaction ---------------------------------------------------------------------
	
	// Event Listener on Button[#batchTestButton].onAction
	/** Test the inputs in the batch table and display the results */
	@FXML public void runBatchTest(ActionEvent event) {
		FiniteAutomata fa = aut.getAutomata();
		try {
			for (TableController.TestEntry ts : batchTestTable.getItems()){
				if (ts.getStr() == null){
					continue;
				}
				ts.setResult(fa.accepts(ts.getStr().replace("\u025B", "")));
				
			}
		} catch (NoStartStateException e) {
			Alert alert = new Alert(AlertType.ERROR, "To make a start state: right "
					+ "click on an existing state and select 'Start State'.", ButtonType.OK);
			alert.setHeaderText("Error: No start state found. Strings cannot be tested without a start state.");
			alert.setTitle("Error: No start state found");
			alert.showAndWait();
		}
	}
	
	// Event Listener on Button[#clearBatchButton].onAction
	/** Clear the batch test table */
	@FXML public void clearBatchTests(ActionEvent event) {
		batchTestTable.getColumns().clear();
		TableController.initializeBatchTable(batchTestTable);
	}
	
	
	/** Show an alert indicating there is no start state */
	private void showStartStateError(){
		Alert alert = new Alert(AlertType.ERROR, "To make a start state: right "
				+ "click on an existing state and select 'Make Start State'.", ButtonType.OK);
		alert.setHeaderText("Error: No start state found. Strings cannot be tested without a start state.");
		alert.setTitle("Error: No start state found");
		alert.showAndWait();
	}
	
	
	
	
	// Step testing methods ---------------------------------------------------------------------
	
	
	// Block to initialize a listener to disable the unusable step buttons
	{
			
	index.addListener(new ChangeListener<Number>(){
		@Override
		public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
			String s = testStringBox.getText();
			backStepButton.setDisable((int)newValue <= 0);
			stepButton.setDisable((int)newValue >= s.length());
		}
	});	
		
		
	}
	
	// Event Listener on Button[#testStringButton].onAction
	/** 
	 * Begins a step test.  This locks the test and editor controls, displays the
	 * test materials, and begins coloring the automatas current states
	 */
	@FXML public void startStepTest(ActionEvent event) {
		
		// Show warning if there is no start state and abort the test
		if (aut.getStartState() == null){
			showStartStateError();
			return;
		}
		
		oldInteractState = interactState;
		interactState = InteractState.TESTING;
		
		// get the text from testStringBox
		lockTestControls(true);
		showTestMaterial(true);
		
		
		Set<State> startingStates = new HashSet<>();
		startingStates.add(aut.getStartState().getState());
		
		String str = testStringBox.getText();
		
		// If the test string is empty, replace it with epsilon
		if (str.length() == 0 || str.equals("\u025B")){
			testStringBox.setText("\u025B");
			// Add a character indicating the current character
			testStringLabel.setText("\u02F0"+"\u025B");
			
		// Otherwise step from the start state with epsilon
		} else {
			// Add a character indicating the current character
			testStringLabel.setText("\u02F0" + str);
			startingStates.addAll(aut.step(null,startingStates));
		}
		
		stateHistory.add(startingStates);
		aut.colorStates(startingStates);
		

		// Refreshes index to update listeners
		index.set(1);
		index.set(0);
		
		// Disable the editor so the user cannot change the state
		editorToolBar.setDisable(true);
	}
	
	
	
	// Event Listener on Button[#stepButton].onAction
	/** 
	 * Uses the next character of the test string to step the 
	 * machine, recoloring its current states
	 */
	@FXML public void stepThroughTest(ActionEvent event) {
		
		// If the index is less than the string length, step using the next character
		if (index.get() < testStringBox.getText().length()){
			
			// Get the next character
			Character c = testStringBox.getText().charAt(index.get());
			
			// Get the states reached by stepping with that character,
			// or using null if the character is epsilon.
			Set<State> states;
			if (c.equals('\u025B')){
				states = (aut.step(null,stateHistory.peek()));
			} else {
				states = (aut.step(c,stateHistory.peek()));			
			}
			
			// Add the states to the history of states
			stateHistory.add(states);
			aut.colorStates(states);
			index.set(index.get()+1);
			
			// Set the result if we stepped to no states or if the string finished
			boolean endOfString = (index.get() == testStringBox.getText().length());
			if (endOfString	&& aut.getAutomata().containsAcceptState(stateHistory.peek())){
				setResultString(true);
				testResult.setVisible(true);
			} else if (endOfString || stateHistory.peek().isEmpty()){
				setResultString(false);
				testResult.setVisible(true);
			} else {
				testResult.setVisible(false);
			}
			
			// Move the test head character to the new index
			StringBuilder sb = new StringBuilder(testStringBox.getText())
					.insert(index.get(),"\u02F0");
			testStringLabel.setText(sb.toString());
			
			
		// The user shouldn't have been able to use the button if the index is beyond the string
		} else {
			throw new AssertionError("You shouldn't have been able to click that...");	
		}
		
	}
	
	
	
	// Event Listener on Button[#backStepButton].onAction
	/** 
	 * Steps backwards through the string
	 */
	@FXML public void stepBackThroughTest(ActionEvent event) {
		if (index.get() > 0){
			stateHistory.pop();
			aut.colorStates(stateHistory.peek());
			index.set(index.get()-1);

			// Set the result if we stepped back to no states
			if (stateHistory.peek().isEmpty()){
				testResult.setVisible(true);
				setResultString(false);
			} else {
				testResult.setVisible(false);
			}

		// The user shouldn't have been able to step back if the 
		// index is index is 0 or below
		} else {
			throw new AssertionError("You shouldn't have been able to click that...");	
		}
		
		// Move the test head character to the new index
		StringBuilder sb = new StringBuilder(testStringBox.getText())
				.insert(index.get(),"\u02F0");
		testStringLabel.setText(sb.toString());
	}
	
	
	// Event Listener on Button[#resetStringButton].onAction
	/** 
	 * Resets the step test unlocking the editor and test controls,
	 * and hiding the test materials.
	 */
	@FXML public void resetStepTest(ActionEvent event) {
		
		stateHistory.clear();
		aut.colorStates(null);
		lockTestControls(false);
		
		showTestMaterial(false);
		testResult.setVisible(false);
		
		editorToolBar.setDisable(false);
		interactState = oldInteractState;
	}
	
	/**
	 * Prevents the user from interacting with the test controls.
	 * @param lock		whether or not to lock the controls
	 */
	public void lockTestControls(boolean lock){
		testStringBox.setEditable(!lock);
		testStringBox.setOpacity((lock) ? 0.5 : 1);
		testStringButton.setDisable(lock);
	}
	
	/**
	 * Change the visibility of the step testing materials.
	 * @param visible	whether or not the materials should be visible
	 */
	public void showTestMaterial(boolean visible){
		testingBox.setManaged(visible);
		stepButton.setVisible(visible);
		backStepButton.setVisible(visible);
		testStringLabel.setVisible(visible);
		testingText.setVisible(visible);
		testingText.setManaged(visible);
	}
	
	/**
	 * Prepares the result for display to the user.
	 * @param result	whether or not the string was accepted
	 */
	public void setResultString(boolean result){
		if (result){
			testResult.setText("The string was accepted.");
			testResult.setTextFill(Color.GREEN);
		} else {
			testResult.setText("The string was rejected.");
			testResult.setTextFill(Color.RED);
		}
		
	}
	
	
	
	
}
