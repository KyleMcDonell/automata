package falt.controller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextField;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;

/**
 * A class to handle the initialization of the table for batch testing.
 * 
 * @author ncameron
 * @author kmcdonell
 */
public class TableController {

	/** The number of rows of the table. */
	private static int tableRowCount;
	
	/**
	 * Defines a table row entry. Contains an interactive field for the string to be
	 * tested, and a non-interactive field to hold the result.
	 */
	public static class TestEntry {
		
		// the testable string
		private final SimpleStringProperty str;
		// the result
		private final SimpleStringProperty result;
		
		/**
		 * Initializes a TestEntry with the given string.
		 * 
		 * @param s		the string to initialize the str property
		 */
		private TestEntry(String s){
			str = new SimpleStringProperty(s);
			result = new SimpleStringProperty();
		}
		
		/**
		 * Returns the string contained in the str property. This is the 
		 * string the user interacts with and may change to get differing
		 * output.
		 * 
		 * @return	the string contained in the str property
		 */
		public String getStr(){ return str.get(); }
		/**
		 * Sets the str property to the given string.
		 * 
		 * @param s		the string to set the property as
		 */
		public void setStr(String s){ str.set(s); }
		
		/**
		 * Sets the result to display "Accepted" or "Rejected".
		 * 
		 * @param res	whether to display "Accepted" or "Rejected".
		 */
		public void setResult(boolean res){
			if (str.get() == null || str.get().length() == 0){
				return;
			} else if (res){
				result.set("Accepted");
			} else {
				result.set("Rejected");
			}
		}
		
		/**
		 * Returns the str property of the entry. This method is necessary for the 
		 * table to properly refresh.
		 * 
		 * @return		the str SimpleStringProperty of the TestEntry
		 */
		public SimpleStringProperty strProperty(){
			return str;
		}

		/**
		 * Returns the result property of the entry. This method is necessary for 
		 * the table to properly refresh.
		 * 
		 * @return		the result SimpleStringProperty of the TestEntry
		 */
		public SimpleStringProperty resultProperty(){
			return result;
		}
		
	}
	
	/**
	 * Defines a table cell that is meant to commit changes on refocus, but does
	 * not currently have that feature. It should be noted that this is a
	 * documented issue with JavaFX and not a product of shoddy coding (ours, at 
	 * least).
	 * 
	 * Currently saves edits on ENTER press, as well as refocusing to most other
	 * places, but will discard edits if the user refocuses to any of the pre-existing
	 * cells in the table.
	 *
	 */
	private static class TestingCell extends TableCell<TestEntry,String> {
		
		// holds the text in the cell
		private TextField textField;
	    
		public TestingCell(){}

		private TextField getTextField(){ return textField; }
		
		private String getString() {
			return getItem() == null ? "" : getItem().toString();
		}
	    
		/**
		 * Begins an edit process.
		 */
		@Override
		public void startEdit() {
			if(!isEmpty()){
				super.startEdit();
				createTextField();
				setText(null);
				setGraphic(textField);
				textField.selectAll();
			}
		}
		
		/**
		 * Cancels an edit process.
		 */
		@Override
		public void cancelEdit() {
			super.cancelEdit();
			setText((String) getItem());
			setGraphic(null);
		}
		
		/**
		 * Updates the information in the TestingCell.
		 */
		@Override
		public void updateItem(String item, boolean empty) {
			super.updateItem(item, empty);
			
			if (empty) {
				setText(null);
				setGraphic(null);
			} else {
				if (isEditing()) {
					if (textField != null)
						textField.setText(getString());
					setText(null);
					setGraphic(textField);
				} else {
					setText(getString());
					setGraphic(null);
				}
			}

		}
		
		/**
		 * Creates the textField and adds appropriate listeners.
		 */
		private void createTextField() {
			textField = new TextField(getString());
			textField.setMinWidth(this.getWidth() - this.getGraphicTextGap()*2);
			textField.focusedProperty().addListener(new ChangeListener<Boolean>(){
					@Override
					public void changed(ObservableValue<? extends Boolean> arg0,
							Boolean arg1, Boolean arg2) {
								if (!arg2) {
									//textField.requestFocus();
									commitEdit(textField.getText());
								}
							}
			});
			textField.setOnKeyPressed(new EventHandler<KeyEvent>(){
					@Override
					public void handle(KeyEvent keyEvent){
						if (keyEvent.getCode() == KeyCode.ENTER){
							commitEdit(textField.getText());
						}
					}
			});
			textField.textProperty().addListener(new ChangeListener<String>(){
				@Override
				public void changed(ObservableValue<? extends String> arg0,
						String oldValue, String newValue){
					
					// Replace spaces with epsilon
					if (newValue.contains(" ")){
						textField.setText(newValue.replace(" ", "\u025B"));
					}
					
				}
			});
		}

	}
	
	/**
	 * Handles initialization of the batch testing table.
	 */
	public static void initializeBatchTable(TableView<TestEntry> batchTestTable){
		
		javafx.collections.ObservableList<TestEntry> data = 
				FXCollections.observableArrayList();
		
		tableRowCount = 1;
		for (int i=0; i<tableRowCount; i++){
			data.add(new TestEntry(null));
		}

		// create the cell factory of custom-editable cells
		Callback<TableColumn<TestEntry,String>,TableCell<TestEntry,String>> editableCellFactory = 
				new Callback<TableColumn<TestEntry,String>,TableCell<TestEntry,String>>(){
					public TableCell<TestEntry,String> call(TableColumn<TestEntry,String> c){
						TestingCell tc = new TestingCell();
						tc.createTextField();
						tc.setOnMouseReleased(new EventHandler<MouseEvent>(){
							@Override
							public void handle(MouseEvent mouseEvent){
								
								tc.getTextField().requestFocus();
							}
						});
						return tc;
					}
		};
		

		TableColumn<TestEntry,String> testColumn = new TableColumn<>("Test Input");
		testColumn.setSortable(false);
		testColumn.setMinWidth(-1.0);
		testColumn.setPrefWidth(100.0);
		testColumn.setCellValueFactory(
				new PropertyValueFactory<TestEntry,String>("str")
		);
		testColumn.setOnEditCommit(
				new EventHandler<CellEditEvent<TestEntry,String>>() {
					@Override
					public void handle(CellEditEvent<TestEntry,String> t) {
						int rowIndex = t.getTablePosition().getRow();
						
						((TestEntry) t.getTableView().getItems().get(rowIndex))
						.setStr(t.getNewValue());
	
						// here is where the code should go to make a new one if we just edited the last of them
						if (t.getTablePosition().getRow() == tableRowCount-1 
								&& t.getNewValue() != null){
							TestEntry testEntry = new TestEntry(null);
							data.add(testEntry);
							TableController.tableRowCount++;
						}
						if (t.getTableView().getItems().get(rowIndex).getStr()
								.length() == 0 && rowIndex != 0){
							data.remove(rowIndex);
							TableController.tableRowCount--;
						}
						if (rowIndex > 1 && t.getTableView().getItems()
								.get(rowIndex-1).getStr().length() == 0){
							data.remove(rowIndex-1);
							TableController.tableRowCount--;
						}
						
					}
				});
		testColumn.setCellFactory(editableCellFactory);

		TableColumn<TestEntry,String> resultColumn = new TableColumn<>("Results");
		resultColumn.setSortable(false);
		resultColumn.setEditable(false);
		resultColumn.setResizable(false);
		resultColumn.setMinWidth(100.0);
		resultColumn.setMaxWidth(100.0);
		resultColumn.setPrefWidth(100.0);
		resultColumn.setCellValueFactory(
				new PropertyValueFactory<TestEntry,String>("result")
		);
		resultColumn.setOnEditCommit(
				new EventHandler<CellEditEvent<TestEntry,String>>() {
					@Override
					public void handle(CellEditEvent<TestEntry,String> t) {
						((TestEntry) t.getTableView().getItems().get(
							t.getTablePosition().getRow())
							).setStr(t.getNewValue());
					}
				});
		resultColumn.setCellFactory(editableCellFactory);
		
		
		batchTestTable.getColumns().addAll(testColumn, resultColumn);
		batchTestTable.setItems(data);
		batchTestTable.setEditable(true);
	}
}
