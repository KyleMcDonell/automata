package falt.controller;

import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyBooleanWrapper;

/**
 * This action pattern manager stores actions in a linked list
 * and allows operations to be undone or redone.
 * 
 * @author ncameron
 * @author kmcdonell
 */
public class UndoManager {

	/** The last executed action managed by the UndoManager */
	private Node currentAction;
	/** The head of the linked list which contains no action */
	private Node noAction;
	/** 
	 * A pointer indicating the action after which the last save
	 * occurred.
	 */
	private Node lastSaved;
	

	/** 
	 * Boolean properties indicating whether or not the UndoManager can undo,
	 * can redo, or is in its last saved state, respectively.	 
	 */
	private ReadOnlyBooleanWrapper canUndoProperty, canRedoProperty, savedProperty;
	
	/**
	 * Create a new UndoManager with no action.
	 */
	public UndoManager(){
		noAction = new Node(null);
		currentAction = noAction;
		canUndoProperty = new ReadOnlyBooleanWrapper();
		canRedoProperty = new ReadOnlyBooleanWrapper();
		savedProperty = new ReadOnlyBooleanWrapper();
		updateProperties();
	}
	
	/**
	 * Create a new UndoManager using the sequence of actions
	 * managed by the provided manager.
	 * @param manager	the manager on which to base this UndoManager
	 */
	public UndoManager(UndoManager manager){
		currentAction = manager.currentAction;
		noAction = manager.noAction;
		lastSaved = manager.noAction;
		canUndoProperty = new ReadOnlyBooleanWrapper();
		canRedoProperty = new ReadOnlyBooleanWrapper();
		savedProperty = new ReadOnlyBooleanWrapper();
		updateProperties();
	}
	
	/**
	 * Clears the UndoManager of all actions
	 */
	public void clear(){
		currentAction = noAction;
		noAction.next = null;
		updateProperties();
	}
	
	/**
	 * Saves the UndoManager, updating its last saved action
	 */
	public void save(){
		lastSaved = currentAction;
		updateProperties();
	}
	
	
	/**
	 * Adds a new action to the UndoManager. Note this action should be
	 * executed before being added.  If the UndoManager is not currently
	 * at the last executed action, the remaning chain of actions will be 
	 * lost and replaced by this one.
	 * 
	 * @param c		the action to add
	 */
	public void addAction(ActionPattern c){
		if (c == null)
			throw new IllegalArgumentException("Null cannot be added to the UndoManager");
		Node old = currentAction;
		old.next = new Node(c);
		currentAction = currentAction.next;
		currentAction.prev = old;
		updateProperties();
	}
	
	/**
	 * Undoes the current action in the UndoManager and moves the manager
	 * backwards.  
	 * 
	 * @throws IllegalStateException if there is no action to undo
	 */
	public void undo() throws IllegalStateException {
		if (!getCanUndo())
			throw new IllegalStateException("There are no commands to undo.");
		currentAction.command.undo();
		currentAction = currentAction.prev;
		updateProperties();
	}
	
	/**
	 * Undoes the current action in the UndoManager and moves the manager
	 * forwards
	 * 
	 * @throws IllegalStateException if there is no action to redo
	 */
	public void redo() throws IllegalStateException {
		if (!getCanRedo())
			throw new IllegalStateException("There are no commands to redo.");
		currentAction = currentAction.next;
		currentAction.command.redo();
		updateProperties();
	}
	
	
	/**
	 * Updates the canUndo, canRedo, and saved properties to reflect
	 * the UndoManager's current position
	 */
	private void updateProperties(){
		canUndoProperty.set(currentAction != noAction);
		canRedoProperty.set(currentAction.next != null);
		savedProperty.set(currentAction == lastSaved);
	}
	
	/**
	 * Gets whether or not the UndoManager has an action to undo
	 * @return		whether there is an action to undo
	 */
	public boolean getCanUndo(){ return canUndoProperty.get();	}
	
	/**
	 * Gets whether or not the UndoManager has an action to redo
	 * @return		whether there is an action to redo
	 */
	public boolean getCanRedo(){ return canRedoProperty.get();	}
	
	/**
	 * Gets whether or not the UndoManager is in its last saved
	 * state
	 * @return		whether the UndoManager is saved
	 */
	public boolean getSaved(){ return savedProperty.get();	}
	
	/**
	 * Gets a ReadOnlyBooleanProperty indicating whether or not
	 * the UndoManager has an action to undo.
	 * 
	 * @return 		the property indicating whether there is an 
	 * 				action to undo
	 */
	public ReadOnlyBooleanProperty canUndoProperty(){
		return canUndoProperty.getReadOnlyProperty();
	}
	
	/**
	 * Gets a ReadOnlyBooleanProperty indicating whether or not
	 * the UndoManager has an action to redo.
	 * 
	 * @return 		the property indicating whether there is an 
	 * 				action to redo
	 */
	public ReadOnlyBooleanProperty canRedoProperty(){
		return canRedoProperty.getReadOnlyProperty();
	}
	
	/**
	 * Gets a ReadOnlyBooleanProperty indicating whether or not
	 * the UndoManager is saved
	 * 
	 * @return 		the property indicating whether the UndoManager
	 * 				is saved
	 */
	public ReadOnlyBooleanProperty savedProperty(){
		return savedProperty.getReadOnlyProperty();
	}
	
	
	/** Node class for the underlying linked list structure of the
	 * manager
	 * 
	 * @author ncameron
	 * @author kmcdonell
	 */
	private static class Node {
		
		private Node next = null;
		private Node prev = null;
		
		private final ActionPattern command;
		
		public Node(ActionPattern c){
			command = c;
		}
		
	}
}
