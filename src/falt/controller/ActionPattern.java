package falt.controller;

/**
 * ActionPatterns are actions which can be undone or redone.
 * This generally assumes no actions are done between undoing or redoing.
 * 
 * @author ncameron
 * @author kmcdonell
 */
public interface ActionPattern {

	/**
	 * Implements redo functionality.
	 */
	void redo();
	
	/**
	 * Implements undo functionality.
	 */
	void undo();
	
}
