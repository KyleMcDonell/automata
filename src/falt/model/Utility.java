package falt.model;

/**
 * Provides a static method to ensure non null arguments.
 * 
 * @author ncameron
 * @author kmcdonell
 */
public final class Utility {
	
	/*
	 * Private constructor so no instances can be made.
	 */
	private Utility(){ }
	
	/**
	 * Throws an exception if any of the passed arguments are null.
	 * 
	 * @param objects		the arguments to test
	 */
	public static void nonNullArguments(Object... objects){
		for (Object o: objects){
			if (o == null)
				throw new IllegalArgumentException("Null is an illegal argument");
		}
	}
}
