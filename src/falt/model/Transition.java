package falt.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A transition between finite automata states. Transitions on different
 * characters between the same two states are considered one transition in this
 * implementation. Each transition maintains a set of characters on which it is
 * able to occur, with null having the same functionality as the empty string. 
 * 
 * @author ncameron
 * @author kmcdonell
 */
public class Transition implements Serializable {

	private static final long serialVersionUID = -1710735609443712380L;
	/** The source state for this transition. */
	private final State source;
	/** The target state for this transition. */
	private final State target;
	/** The set of characters on which the transition occurs. */
	private Set<Character> characters = new HashSet<>();
	
	/**
	 * Creates a new transition with no characters between two states. Throws an
	 * IllegalArgumentException if either of the states are null.
	 * 
	 * @param source	the state where the transition originates from
	 * @param target	the state where the transition goes to
	 */
	public Transition(State source, State target) {
		Utility.nonNullArguments(source, target);
		this.source = source;
		this.target = target;
	}
	
	/**
	 * Returns the source of a Transition.
	 * @return		the source State of a Transition
	 */
	public State getSource() { return source; }
	
	/**
	 * Returns the target of a Transition.
	 * @return		the target State of a Transition
	 */
	public State getTarget() { return target; }
	
	/**
	 * Returns whether or not this transition contains any characters.
	 * @return		if the transition contains no characters
	 */
	public boolean isEmpty() { return characters.isEmpty(); }
	
	/**
	 * Expands the Transition so that it may occur on the character specified.
	 * 
	 * @param c		the new character for the Transition
	 * @return		true if successful, false if there was already a transition
	 * 				on that character.
	 */
	public boolean addCharacter(Character c) { 
		boolean result = characters.add(c);
		return result;
	}
	
	
	/**
	 * Adds each character in the provided set to the transition.  If a character was
	 * already included in the transition, it is ignored.
	 * 
	 * @param c		a set of characters to be added to the transition
	 * @return		the set of new characters which were actually added to the transition
	 */
	public Set<Character> addCharacters(Set<Character> chars){
		Utility.nonNullArguments(chars);
		Set<Character> newChars = new HashSet<>(chars);
		newChars.removeAll(characters);
		characters.addAll(chars);
		return newChars;
	}
	
	/**
	 * Removes a character from a Transition so that it may no longer occur when
	 * given the specified character.
	 * 
	 * @param c		the character to remove from the Transition
	 * @return		true if successful, else false
	 */
	public boolean removeCharacter(Character c) { return characters.remove(c); }
	
	
	/**
	 * Removes each character in the provided set from the transition.  If a character was
	 * not included in the transition, it is ignored.
	 * 
	 * @param c		a set of characters to be removed from the transition
	 * @return		the set of characters which were actually removed from the transition
	 */
	public Set<Character> removeCharacters(Set<Character> chars){
		Utility.nonNullArguments(chars);
		Set<Character> newChars = new HashSet<>(chars);
		newChars.retainAll(characters);
		characters.removeAll(chars);
		return newChars;
	}
	
	/**
	 * Changes set of characters included in the transition.  Note that modifications of
	 * the provided set will result in changes to the transition.
	 * 
	 * @param c		the set of characters the transition will include
	 * @return		the set of characters the transition previously included
	 */
	public Set<Character> setCharacters(Set<Character> chars){
		Utility.nonNullArguments(chars);
		Set<Character> oldChars = characters;
		characters = chars;
		return oldChars;
	}
	
	
	/**
	 * Returns a Set containing the characters of the transition.
	 * 
	 * @return		a Set containing the characters of the transition
	 */
	public Set<Character> getCharacters() { return characters; }
	
	/**
	 * Checks if the transition can occur for a specified character.
	 * 
	 * @param c		the character to check on
	 * @return		true if the transition can occur on the character, else false.
	 */
	public boolean containsCharacter(Character c) { return characters.contains(c); }
	
	/**
	 * Returns a count of the number of characters for which the transition may
	 * occur.
	 * 
	 * @return		the number of characters of the transition
	 */
	public int countCharacters() { return characters.size(); }
	
}
