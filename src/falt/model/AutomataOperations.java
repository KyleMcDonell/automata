package falt.model;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

/**
 * A class containing static methods to combine finite automata with the operations
 * union, concatenation, Kleene star, intersection, and complement. These operations
 * give finite automata that accept the language that is the result of applying the 
 * operation to source automatas' languages. 
 * 
 * These operations can optionally return a correspondence between the source(s) and 
 * the resulting finite automata.
 * 
 * This class also contains other static methods for checking the equivalence of automata,
 * checking for language emptiness, and minimizing DFAs.
 * 
 * @author ncameron
 * @author kmcdonell
 */
public class AutomataOperations {
	
	
	/**
	 * Returns a finite automata that accepts the union of two given finite 
	 * automata's languages.
	 * 
	 * @param fa1	the first of the two FiniteAutomata in the union
	 * @param fa2	the second of the two FiniteAutomata in the union
	 * @return		the resulting FiniteAutomata union
	 */
	public static FiniteAutomata union(FiniteAutomata fa1, FiniteAutomata fa2) {
		return unionWithCorrespondence(fa1,fa2).target;
	}
	
	
	/**
	 * Returns a correspondence between the source finite automata of a union 
	 * operation, and the resulting finite automata that accepts the union of the
	 * two given finite automata's languages.
	 * 
	 * @param fa1	the first of the two FiniteAutomata in the union
	 * @param fa2	the second of the two FiniteAutomata in the union
	 * @return		the correspondence between the source and new union 
	 * 				FiniteAutomata
	 */
	public static AutomataCorrespondence unionWithCorrespondence(FiniteAutomata fa1, 
			FiniteAutomata fa2) {
		
		FiniteAutomata unionFA = new FiniteAutomata();
		AutomataCorrespondence fa1Copy = fa1.copyWithCorrespondence();
		AutomataCorrespondence fa2Copy = fa2.copyWithCorrespondence();
		
		List<State> newStates = new ArrayList<>();
		
		// Add a new start state
		State start = new State();
		unionFA.addState(start);
		unionFA.setStart(start);
		newStates.add(start);
		
		// copy states
		for(State st: fa1Copy.target.getStates()) {
			unionFA.addState(st);
			if(st == fa1Copy.target.getStart())
				start.addTransitionCharacter(st, null);
		}
		for(State st: fa2Copy.target.getStates()) {
			unionFA.addState(st);
			if(st == fa2Copy.target.getStart()) 
				start.addTransitionCharacter(st, null);
		}
		
		// Get the correspondence map by taking the union of the correspondence map 
		// for each of the two finite automata
		Map<State,State> correspondence = fa1Copy.stateMap;
		correspondence.putAll(fa2Copy.stateMap);
		
		return new AutomataCorrespondence(fa1,fa2,
				correspondence,newStates,unionFA);
		
	}
	
	
	
	
	/**
	 * Returns a FiniteAutomata that accepts the concatenation of two finite automata's
	 * languages.
	 * 
	 * @param fa1	the first of the two FiniteAutomata in the concatenation
	 * @param fa2	the second of the two FiniteAutomata in the concatenation
	 * @return		the resulting FiniteAutomata concatenation
	 */
	public static FiniteAutomata concatenation(FiniteAutomata fa1, FiniteAutomata fa2) {
		return concatenationWithCorrespondence(fa1,fa2).target;
	}
	
	/**
	 * Returns a correspondence between the source finite automata of a concatenation 
	 * operation, and the resulting finite automata that accepts the concatenation of 
	 * the two given finite automata's languages.
	 * 
	 * @param fa1	the first of the two FiniteAutomata in the union
	 * @param fa2	the second of the two FiniteAutomata in the union
	 * @return		the correspondence between the source and new union 
	 * 				FiniteAutomata
	 */
	public static AutomataCorrespondence concatenationWithCorrespondence(
			FiniteAutomata fa1, FiniteAutomata fa2) {
		
		FiniteAutomata concatenationFA = new FiniteAutomata();
		AutomataCorrespondence fa1Copy = fa1.copyWithCorrespondence();
		AutomataCorrespondence fa2Copy = fa2.copyWithCorrespondence();
		
		// Add each state of the copies to the concatenation
		// Add an epsilon transition from each final state of fa1 to
		// the start state of fa2
		for(State st: fa1Copy.target.getStates()){
			concatenationFA.addState(st);
			if (st.isAccept()){
				st.addTransitionCharacter(fa2Copy.target.getStart(), null);
				st.setAccept(false);
			}
			if (st==fa1Copy.target.getStart()){
				concatenationFA.setStart(st);
			}
		}
		
		for(State st: fa2Copy.target.getStates()){
			concatenationFA.addState(st);
		}
		
		// Get the correspondence map by taking the union of the correspondence map 
		// for each automata
		Map<State,State> correspondence = fa1Copy.stateMap;
		correspondence.putAll(fa2Copy.stateMap);
		
		return new AutomataCorrespondence(fa1,fa2,
				correspondence,new ArrayList<State>(),concatenationFA);
	}
	
	
	/**
	 * Returns a FiniteAutomata that accepts the intersection of two finite automata's
	 * languages. Has no equivalent correspondence method as there is no way to map a
	 * state from a source finite automata to a state in the target intersection.
	 * 
	 * @param fa1	the first of the two FiniteAutomata to be intersected
	 * @param fa2	the second of the two FiniteAutomata to be intersected
	 * @return		the resulting FiniteAutomata intersection
	 */
	public static FiniteAutomata intersection(FiniteAutomata fa1, FiniteAutomata fa2) {
		
		FiniteAutomata intersectionFA = new FiniteAutomata();
		FiniteAutomata fa1copy = fa1.copy();
		FiniteAutomata fa2copy = fa2.copy();
		
		HashMap<Map.Entry<State,State>, State> intersectionStates = new HashMap<>();
		// entries will be keys for new states for the intersectionFA
		// this is the Cartesian product of the states
		for(State st1: fa1copy.getStates()){
			for(State st2: fa2copy.getStates()){
				
				// Create a state for the pair
				State st1st2 = new State();
				
				// Accept only if both states are
				if (st1.isAccept() && st2.isAccept())
					st1st2.setAccept(true);
				
				// Add the state to the hashmap and intersection FA
				intersectionStates.put(
						new AbstractMap.SimpleEntry<State,State>(st1, st2), st1st2);
				intersectionFA.addState(st1st2);
			}
		}
		
		// Sets the start state of the intersection FA 
		AbstractMap.SimpleEntry<State,State> startEntry = 
				new AbstractMap.SimpleEntry<>(fa1copy.getStart(),fa2copy.getStart());
		intersectionFA.setStart(intersectionStates.get(startEntry));
		
		
		/* this will create the transitions */
		for(Map.Entry<State, State> entryA: intersectionStates.keySet()) {
			// create transitions from intersectStateA to intersectStateB if there
			// should be any. that is determined by the transitions between st1a &
			// st1b, AND the transitions between st2a & st2b (each of those pairs
			// is the index for a state in the intersection). Remember the states
			// with the same NUMBERS are from the same FAs.
			State st1a = entryA.getKey();
			State st2a = entryA.getValue();
			
			State intersectStateA = intersectionStates.get(entryA);
			
			for(Map.Entry<State, State> entryB: intersectionStates.keySet()) {
				State st1b = entryB.getKey();
				State st2b = entryB.getValue();
				
				State intersectStateB = intersectionStates.get(entryB);
				
				// if st1a can go to st1b and st2a can go to st2b, i.e. if the intersection
				// states should have a transition
				if (st1a.containsTransition(st1b) && st2a.containsTransition(st2b)){
					Transition t1 = st1a.getTransition(st1b);
					Transition t2 = st2a.getTransition(st2b);
					
					Set<Character> t1Alphabet = t1.getCharacters();
					Set<Character> t2Alphabet = t2.getCharacters();
					
					// if on any character we can transition to both st1b and st2b then add a transition
					// to the intersection FA
					if(!Collections.disjoint(t1Alphabet, t2Alphabet)){
						
						t1Alphabet.retainAll(t2Alphabet);
						Set<Character> charactersInBoth = t1Alphabet;
						
						intersectStateA.addTransitionSet(intersectStateB, charactersInBoth);
					}
				}
			}
		}
		
		return intersectionFA;
	}
	
	
	
	/**
	 * Returns a FiniteAutomata that accepts the Kleene star applied to a given
	 * finite automata's language.
	 * 
	 * @param fa1	the FiniteAutomata to apply the Kleene star operator to
	 * @return		the resulting star FiniteAutomata
	 */
	public static FiniteAutomata star(FiniteAutomata fa1) {
		return starWithCorrespondence(fa1).target;
	}
	
	
	/**
	 * Returns a correspondence between the source finite automata of a star 
	 * operation, and the resulting finite automata that accepts the star operation
	 * applied to the source finite automata's language.
	 * 
	 * @param fa1	the FiniteAutomata to apply the star operator to
	 * @return		the correspondence between the source and new star FiniteAutomata
	 */
	public static AutomataCorrespondence starWithCorrespondence(FiniteAutomata fa1) {
		AutomataCorrespondence starFA = fa1.copyWithCorrespondence();

		State start = new State();
		// add empty string transitions from old accept states to new accept/start state
		for(State st: starFA.target.getStates()){
			if (st.isAccept()){
				st.setAccept(false);
				st.addTransitionCharacter(start, null);
			}
		}

		start.addTransitionCharacter(starFA.target.getStart(), null);
		start.setAccept(true);
		starFA.target.addState(start);
		starFA.target.setStart(start);
		
		List<State> newStates = new ArrayList<>();
		newStates.add(start);
		
		
		return new AutomataCorrespondence(fa1,
				starFA.stateMap,newStates,starFA.target);
	}
	
	
	
	
	
	/**
	 * Returns a FiniteAutomata that accepts the complement of a given finite 
	 * automata's language. The complement is taken only within the alphabet
	 * of the finite automata.
	 * 
	 * @param fa1	the FiniteAutomata to find the complement of
	 * @return		the resulting complement FiniteAutomata
	 */
	public static FiniteAutomata complement(FiniteAutomata fa1) {
		return complementWithCorrespondence(fa1).target;
	}
	
	
	/**
	 * Returns a correspondence between the source finite automata of a complement 
	 * operation, and the resulting finite automata that accepts the complement of 
	 * the given finite automata's language.
	 * 
	 * @param fa1	the FiniteAutomata to find the complement of
	 * @return		the correspondence between the source and new complement 
	 * 				FiniteAutomata
	 */
	public static AutomataCorrespondence complementWithCorrespondence(
			FiniteAutomata fa1) {
		
		AutomataCorrespondence complementFA = fa1.copyWithCorrespondence();
		Set<State> states = complementFA.target.getStates();
		Set<Character> alphabet = complementFA.target.getAlphabet();
		
		// create the fail sink, later turned into an accept sink
		State sink = new State();
		for(Character ch: alphabet){
			sink.addTransitionCharacter(sink, ch);
		}
		complementFA.target.addState(sink);
		
		// adds transitions to the sink
		for(Character ch: alphabet){
			// don't add empty transitions to the sink
			if (ch == null)
				continue;
			for(State st1: states){
				if(!st1.containsTransition(ch)){
					st1.addTransitionCharacter(sink, ch);
				}
			}
		}
		
		// Flip the acceptance of all states
		for(State st: states){
			st.setAccept(!st.isAccept());
		}
		
		List<State> newStates = new ArrayList<>();
		newStates.add(sink);
		
		return new AutomataCorrespondence(fa1,
				complementFA.stateMap,newStates,complementFA.target);
	}
	
	/**
	 * Returns whether or not the given finite automata accepts the empty language.
	 * 
	 * @param automata		the FiniteAutomata to check the accepting language
	 * @return				true if automata accepts only the empty language
	 */
	public static boolean acceptsEmptyLanguage(FiniteAutomata automata){
		
		// Reset the mark of every state in the automata
		for (State s : automata.getStates())
			s.setMark(false);
		
		// Add all of the acceptStates to a queue and mark them.
		// Return false if any are accept states
		Queue<State> queue = new LinkedList<>(automata.getAcceptStates());
		for (State s : queue){
			if (automata.getStart() == s)
				return false;
			else
				s.setMark(true);
		}
		
		// Follow transitions backwards from the accept states and
		// return true only if the start state cannot be reached
		while (!queue.isEmpty()){
			
			State currentState = queue.remove();
			
			// Add all states which can transition to the current state to the queue
			for (State s : automata.getStates()){
				Transition t = s.getTransition(currentState);
				if (t != null && !t.isEmpty()){
					// If a state which can be reached is the start state, return false
					if (automata.getStart() == s){
						return false;
					
					// Otherwise, if the state is not marked, add it to the queue and mark it
					} else if (!s.getMark()){
						s.setMark(true);
						queue.add(s);
					}
				}
			}		
		}
		return true;
	}
	
	/**
	 * Returns whether or not two given finite automata accept the same language.
	 * This is done by using the symmetric difference, and testing for emptiness.
	 * 
	 * @param fa1	the first FiniteAutomata to compare
	 * @param fa2	the second FiniteAutomata to compare
	 * @return		true if fa1 and fa2 accept the same language, else false
	 */
	public static boolean checkEquivalence(FiniteAutomata fa1, FiniteAutomata fa2){
		
		Set<Character> alphabet1 = fa1.getAlphabet();
		Set<Character> alphabet2 = fa2.getAlphabet();
		
		// If both alphabets are empty, return true
		// This is for optimization
		if (alphabet1.isEmpty() && alphabet2.isEmpty())	
			return true;
		
		// If the alphabets are not equal, return false
		// This check is required as if the alphabets are disjoint
		// the symmetric difference will be empty
		if (!alphabet1.containsAll(alphabet2) 
				|| !alphabet2.containsAll(alphabet1))
			return false;
		
		// If one is empty and the other is not, return false
		// This is for optimization as the intersection of large FAs is
		// very large and should be avoided
		if (acceptsEmptyLanguage(fa1) != acceptsEmptyLanguage(fa2))
			return false;
		
		
		// Otherwise, look at the symmetric difference
		FiniteAutomata fa1Intersectionfa2C = intersection(fa1,complement(fa2));
		FiniteAutomata fa2Intersectionfa1C = intersection(fa2,complement(fa1));
		FiniteAutomata symmetricDifference = union(fa2Intersectionfa1C,fa1Intersectionfa2C);
		return acceptsEmptyLanguage(symmetricDifference);
	}
	
	/**
	 * Returns whether or not a given finite automata is a deterministic finite
	 * automata.
	 * 
	 * @param automata		the FiniteAutomata to check for non-determinism
	 * @return				true if automata is a DFA, else false
	 */
	public static boolean isDeterministic(FiniteAutomata automata){
		
		// Checks if there are any states with multiple transitions on
		// a character or a transition on epsilon
		Set<Character> characters = new HashSet<Character>();
		for(State st : automata.getStates()){
			for(Transition t : st.getTransitions()){
				for(Character c : t.getCharacters()){
					if (c == null || characters.contains(c))
						return false;
					else
						characters.add(c);
				}
			}
			characters.clear();
		}
		return true;
	}
	
	/**
	 * Minimizes a deterministic finite automata.  
	 * 
	 * @param automata		the (deterministic) FiniteAutomata to minimize
	 * @return				null for now, the minimized (deterministic) 
	 * 						FiniteAutomata once implemented
	 */
	public FiniteAutomata minimizeDFA(FiniteAutomata automata){
		
		
		
		
		// is null a DFA?
		return null;
	}
	
	
	
	/**
	 * Testing method.
	 * 
	 * @param args	command line arguments, no effect
	 */
	public static void main(String[] args) {

		// sample FA
		FiniteAutomata epsilon = SampleFiniteAutomata.epsilon();
		FiniteAutomata a = SampleFiniteAutomata.a();
		FiniteAutomata b = SampleFiniteAutomata.b();
		FiniteAutomata aa = SampleFiniteAutomata.aa();
		FiniteAutomata bb = SampleFiniteAutomata.bb();
		FiniteAutomata ab = SampleFiniteAutomata.ab();
		FiniteAutomata a10 = SampleFiniteAutomata.a10();
		FiniteAutomata b10 = SampleFiniteAutomata.b10();
		FiniteAutomata aStar = SampleFiniteAutomata.aStar();
		FiniteAutomata bStar = SampleFiniteAutomata.bStar();
		
		
		// regular operations applied
		FiniteAutomata aa_Union_bb = union(aa, bb);
		
		FiniteAutomata aStar_Union_bStar = union(aStar, bStar);
		FiniteAutomata ab_Union_epsilon = union(ab, epsilon);

		FiniteAutomata a_Concatenation_b = concatenation(a, b);
		FiniteAutomata aStar_Concatenation_b = concatenation(aStar, b);
		FiniteAutomata aa_Concatenation_bStar = concatenation(aa, bStar);
		FiniteAutomata aStar_Concatenation_bStar = concatenation(aStar, bStar);
		
		FiniteAutomata aa_Intersection_aStar = intersection(aa, aStar);
		FiniteAutomata a_Intersection_bStar = intersection(a, bStar);
		FiniteAutomata aStar_Intersection_bStar = intersection(aStar, bStar);
		FiniteAutomata a10_Intersection_a10 = intersection(a10, a10);
		FiniteAutomata a10_Intersection_b10 = intersection(a10, b10);
		FiniteAutomata a_Intersection_aStar = intersection(a,aStar);
		FiniteAutomata a_Intersection_ab = intersection(a,ab);
		
		FiniteAutomata aa_Star = star(aa);
		
		FiniteAutomata ab_Complement = complement(ab);
		FiniteAutomata aStar_Complement = complement(aStar);
		
		
		System.out.println("a10 has start: " + a10.getStart() != null);
		System.out.println("b10 has start: " + b10.getStart() != null);
		System.out.println("a10 int b10 has start: " + a10_Intersection_b10.getStart() != null);
		System.out.println("a10 int a10 has start: " + a10_Intersection_a10.getStart() != null);
		
		// test
		try {
			long start = System.nanoTime();
			// union tests
			System.out.println("\r\nUnion: \r\n");
			System.out.println("TTFFFF:");
			System.out.println(aa_Union_bb.accepts("aa"));
			System.out.println(aa_Union_bb.accepts("bb"));
			System.out.println(aa_Union_bb.accepts("ab"));
			System.out.println(aa_Union_bb.accepts("a"));
			System.out.println(aa_Union_bb.accepts("b"));
			System.out.println(aa_Union_bb.accepts(""));
			
			System.out.println("TTTF:");
			System.out.println(aStar_Union_bStar.accepts("aaaaaaaaaaa"));
			System.out.println(aStar_Union_bStar.accepts(""));
			System.out.println(aStar_Union_bStar.accepts("bbbbbb"));
			System.out.println(aStar_Union_bStar.accepts("ababababba"));

			System.out.println("TTFFF:");
			System.out.println(ab_Union_epsilon.accepts(""));
			System.out.println(ab_Union_epsilon.accepts("ab"));
			System.out.println(ab_Union_epsilon.accepts("a"));
			System.out.println(ab_Union_epsilon.accepts("aaab"));
			System.out.println(ab_Union_epsilon.accepts("b"));
			
			// concatenation tests
			System.out.println("\r\nConcatenation: \r\n");
			System.out.println("TFFFF:");
			System.out.println(a_Concatenation_b.accepts("ab"));
			System.out.println(a_Concatenation_b.accepts("a"));
			System.out.println(a_Concatenation_b.accepts("b"));
			System.out.println(a_Concatenation_b.accepts("bb"));
			System.out.println(a_Concatenation_b.accepts(""));

			System.out.println("TTTFF:");
			System.out.println(aStar_Concatenation_b.accepts("b"));
			System.out.println(aStar_Concatenation_b.accepts("ab"));
			System.out.println(aStar_Concatenation_b.accepts("aaaab"));
			System.out.println(aStar_Concatenation_b.accepts("aa"));
			System.out.println(aStar_Concatenation_b.accepts(""));

			System.out.println("FTTTF:");
			System.out.println(aa_Concatenation_bStar.accepts(""));
			System.out.println(aa_Concatenation_bStar.accepts("aa"));
			System.out.println(aa_Concatenation_bStar.accepts("aab"));
			System.out.println(aa_Concatenation_bStar.accepts("aabbb"));
			System.out.println(aa_Concatenation_bStar.accepts("ab"));
			
			System.out.println("TTTTFF:");
			System.out.println(aStar_Concatenation_bStar.accepts(""));
			System.out.println(aStar_Concatenation_bStar.accepts("a"));
			System.out.println(aStar_Concatenation_bStar.accepts("b"));
			System.out.println(aStar_Concatenation_bStar.accepts("aaabbb"));
			System.out.println(aStar_Concatenation_bStar.accepts("ba"));
			System.out.println(aStar_Concatenation_bStar.accepts("babbbb"));
			
			
			// intersection tests
			System.out.println("\r\nIntersection: \r\n");
			System.out.println("FTF:");
			System.out.println(aa_Intersection_aStar.accepts("a"));
			System.out.println(aa_Intersection_aStar.accepts("aa"));
			System.out.println(aa_Intersection_aStar.accepts("aaa"));

			System.out.println("FFF:");
			System.out.println(a_Intersection_bStar.accepts("a"));
			System.out.println(a_Intersection_bStar.accepts(""));
			System.out.println(a_Intersection_bStar.accepts("bb"));
			
			System.out.println("TFFF:");
			System.out.println(aStar_Intersection_bStar.accepts(""));
			System.out.println(aStar_Intersection_bStar.accepts("a"));
			System.out.println(aStar_Intersection_bStar.accepts("b"));
			System.out.println(aStar_Intersection_bStar.accepts("ab"));

			System.out.println("TFF:");
			System.out.println(a10_Intersection_a10.accepts("aaaaaaaaaa"));
			System.out.println(a10_Intersection_a10.accepts("a"));
			System.out.println(a10_Intersection_a10.accepts(""));
			
			System.out.println("FFF:");
			System.out.println(a10_Intersection_b10.accepts("aaaaaaaaaa"));
			System.out.println(a10_Intersection_b10.accepts("a"));
			System.out.println(a10_Intersection_b10.accepts(""));
			
			System.out.println("FFF:");
			System.out.println(a_Intersection_ab.accepts("a"));
			System.out.println(a_Intersection_ab.accepts("ab"));
			System.out.println(a_Intersection_ab.accepts(""));
			
			System.out.println("TFF:");
			System.out.println(a_Intersection_aStar.accepts("a"));
			System.out.println(a_Intersection_aStar.accepts("aa"));
			System.out.println(a_Intersection_aStar.accepts(""));
			
			// star tests
			System.out.println("\r\nStar: \r\n");
			System.out.println("TTTF:");
			System.out.println(aa_Star.accepts(""));
			System.out.println(aa_Star.accepts("aa"));
			System.out.println(aa_Star.accepts("aaaa"));
			System.out.println(aa_Star.accepts("aaaaa"));
			
			// complement tests
			System.out.println("\r\nComplement: \r\n");
			System.out.println("TTFT:");
			System.out.println(ab_Complement.accepts("a"));
			System.out.println(ab_Complement.accepts("bbbb"));
			System.out.println(ab_Complement.accepts("ab"));
			System.out.println(ab_Complement.accepts(""));
			System.out.println("FTTT:");
			System.out.println(ab_Complement.accepts("ab"));
			System.out.println(ab_Complement.accepts("aaa"));
			System.out.println(ab_Complement.accepts("bbb"));
			System.out.println(ab_Complement.accepts("abab"));
			System.out.println("FFFF:");
			System.out.println(aStar_Complement.accepts(""));
			System.out.println(aStar_Complement.accepts("aa"));
			System.out.println(aStar_Complement.accepts("b"));
			System.out.println(aStar_Complement.accepts("bbb"));
			
			
			// empty test 
			System.out.println("\r\nEmpty: \r\n");
			System.out.println("TFFFT:");
			System.out.println(acceptsEmptyLanguage(a10_Intersection_b10));
			System.out.println(acceptsEmptyLanguage(aStar_Intersection_bStar));
			System.out.println(acceptsEmptyLanguage(aa));
			System.out.println(acceptsEmptyLanguage(ab));
			System.out.println(acceptsEmptyLanguage(aStar_Complement));
			System.out.println("FFFFT:");
			System.out.println(acceptsEmptyLanguage(ab_Complement));
			System.out.println(acceptsEmptyLanguage(a));
			System.out.println(acceptsEmptyLanguage(b));
			System.out.println(acceptsEmptyLanguage(a10));
			System.out.println(acceptsEmptyLanguage(new FiniteAutomata()));
			System.out.println("FFF:");
			System.out.println(acceptsEmptyLanguage(intersection(aStar,complement(bStar))));
			System.out.println(acceptsEmptyLanguage(intersection(complement(aStar),bStar)));
			System.out.println(acceptsEmptyLanguage(union(
					intersection(aStar,complement(bStar)), 
					intersection(complement(aStar),bStar))));
			
			// equivalence tests
			System.out.println("\r\nEquivalence: \r\n");
			System.out.println("TTFFF");
			System.out.println(checkEquivalence(ab_Complement,ab_Complement));
			System.out.println(checkEquivalence(a10_Intersection_b10,a10_Intersection_b10));
			System.out.println(checkEquivalence(a10,b10));
			System.out.println(checkEquivalence(a10_Intersection_b10,aa));
			System.out.println(checkEquivalence(aStar,bStar));
			System.out.println("FFTTFF");
			System.out.println(checkEquivalence(a10,a));
			System.out.println(checkEquivalence(a,aa));
			System.out.println(checkEquivalence(a,a));
			System.out.println(checkEquivalence(bb,bb));
			System.out.println(checkEquivalence(bb,b));
			System.out.println(checkEquivalence(aStar,a));
			
			

			long end = System.nanoTime();
			System.out.println("\r\nRuntime : " + ((end-start)/1000000.0) + " ms");
			
		} catch (NoStartStateException e) {
			e.printStackTrace();
		}
		
		
		
	}
	
}