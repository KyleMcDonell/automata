package falt.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * A representation of a state in a finite automata. States hold exiting
 * transitions which are accessed by the State the transitions travel to.
 * 
 * @author ncameron
 * @author kmcdonell
 */
public class State implements Serializable {
	
	private static final long serialVersionUID = 4032339594168323475L;
	/** A mapping of the to the transitions to each state. */
	private final Map<State,Transition> transitions = new HashMap<State,Transition>();
	/** A marker specifying accept states. */ 
	private boolean accept;
	/** A marker for misc algorithm purposes. */
	private boolean mark;
	
	/**
	 * Constructs a new state, defaulting to reject and unmarked.
	 */
	public State(){
		mark = false;
		accept = false;
	}
	
	/* ---- Mark methods ---- */
	
	/**
	 * Gets the mark of this state.
	 * @return		the mark value
	 */
	public boolean getMark(){ return mark; }
	
	/**
	 * Changes the mark of this state.
	 * @param m		the new mark
	 * @return		the old mark
	 */
	public boolean setMark(boolean m){ 
		boolean oldMark = this.mark;
		this.mark = m; 
		return oldMark;
	}
	
	/* ---- Accept methods ---- */
	
	/**
	 * Specifies whether this state is an accept state or not.
	 * @return		true if this is an accept state, else false
	 */
	public boolean isAccept(){ return accept; }
	
	/**
	 * Changes the accept/reject property of this state. Pass true to change
	 * the state to an accept state, and false to change the state to a reject
	 * state.
	 * 
	 * @param accept	if the state should accept/reject
	 */
	public void setAccept(boolean accept){ this.accept = accept; }
	
	/* ---- Transition methods ---- */
	
	/**
	 * Returns the number of transitions to other states. Each character on which
	 * we can traverse a Transition is counted as one transition in this method.
	 * For example, if a state has a transition to another state for two characters,
	 * that is counted as two transitions.
	 * 
	 * @return		the number of transitions to other states.
	 */
	public int countTransitions(){ 
		int count = 0;
		for (Transition t : transitions.values())
			count += t.countCharacters();
		return count;
	}
	
	/**
	 * Given a character, follows each legal transition from this state and
	 * returns a set of all states that those transitions lead to.
	 * 
	 * @param c		the given character
	 * @return		the set 
	 */
	public Set<State> followTransition(Character c){
		HashSet<State> states = new HashSet<>();
		for (Transition t : transitions.values()){
			if (t.containsCharacter(c))
				states.add(t.getTarget());
		}
		return states;
	}

	/**
	 * Adds a pre-specified transition between this and another state.
	 * Replaces any pre-existing transitions from this and the other
	 * state.
	 * 
	 * @param state		the state the transition points to
	 * @param t			the transition to add
	 * @return			the replaced transition, or null if none
	 */
	public Transition addTransition(State state, Transition t){
		Utility.nonNullArguments(state);
		return transitions.put(state, t);
	}
	
	/**
	 * Adds a transition between this state and another state which occurs
	 * on a specified character.
	 * 
	 * @param state		the State the transition will point to
	 * @param c			the Character of the transition
	 * @return			true if there is not already such a transition, 
	 * 					else false
	 */
	public boolean addTransitionCharacter(State state, Character c){
		Utility.nonNullArguments(state);
		Transition t = transitions.get(state);
		if (t == null){
			t = new Transition(this, state);
			transitions.put(state, t);
		}
		return transitions.get(state).addCharacter(c);
	}
	
	/**
	 * Adds a transition between this state and another state which occurs
	 * on a specified set of characters.
	 * 
	 * @param state		the state the transition will point to
	 * @param chars		the character of the transition
	 * @return			an array detailing the success of each 
	 * 					character addition.
	 */
	public boolean[] addTransitionSet(State state, Set<Character> chars){
		boolean[] results = new boolean[chars.size()];
		int count = 0;
		for (Character c: chars){
			if(addTransitionCharacter(state, c))
				results[count] = true;
			count++;
		}
		return results;
	}

	/**
	 * Returns a set of the transitions to other states.
	 * 
	 * @return		a Collection of Transitions to other states
	 */
	public Collection<Transition> getTransitions(){ return transitions.values(); }
	
	/**
	 * Returns the transition to a given state from this state. Throws an
	 * IllegalArgumentException if the given state is null.
	 * 
	 * @param state		the state the transition would point to
	 * @return			the transition, null if none exists
	 */
	public Transition getTransition(State state){
		Utility.nonNullArguments(state);
		return transitions.get(state);
	}
	
	/**
	 * Removes all transitions to a given state from this state. Throws an
	 * IllegalArgumentException if the given state is null.
	 * 
	 * @param state		the State the transitions we remove point to
	 * @return			the Transition removed
	 */
	public Transition removeTransition(State state){
		Utility.nonNullArguments(state);
		return transitions.remove(state);
	}
	
	/**
	 * Removes a transition to a state on a character from this state. Updates the
	 * alphabet, if necessary. Throws an IllegalArgumentException if the given
	 * state is null.
	 * 
	 * @param state		the state the transition points to
	 * @param c			the character of the transition
	 * @return			true if successful, else false
	 */
	public boolean removeTransition(State state, Character c){
		Utility.nonNullArguments(state);
		return (transitions.get(state) != null) ? 
				transitions.get(state).removeCharacter(c) : false;
	}
	
	/**
	 * Checks if there is a transition to a given state from this state.
	 * Throws an IllegalArgumentException if the given state is null.
	 * 
	 * @param state		the state we are looking for transitions to
	 * @return			true if any transitions exist, else false
	 */
	public boolean containsTransition(State state){
		Utility.nonNullArguments(state);
		return transitions.containsKey(state);
	}
	
	/**
	 * Checks if there are any transitions away from this state on a specific
	 * character.
	 * 
	 * @param c			the character to check for transitions on
	 * @return			true if there are transitions, false otherwise
	 */
	public boolean containsTransition(Character c){
		for (Transition t : transitions.values()){
			if (t.containsCharacter(c))
				return true;
		}
		return false;
	}
	
	/**
	 * Checks if there is a transition to a state on a character from this state.
	 * Throws an IllegalArgumentException if the state is null.
	 * 
	 * @param state		the state the transition would point to
	 * @param c			the character of the supposed transition
	 * @return			true if such a transition exists
	 */
	public boolean containsTransition(State state, Character c){
		Utility.nonNullArguments(state);
		return (transitions.get(state) != null) ? 
				transitions.get(state).containsCharacter(c) : false;
	}
	
	
	/**
	 * A shell copy of this state. Shell copies are deep copies which exclude 
	 * the transition field.
	 * 
	 * @return		the shell copy of this State
	 */
	public State shellCopy(){
		State copy = new State();
		copy.accept = this.accept;
		copy.mark = this.mark;
		return copy;
	}
	

	
}
