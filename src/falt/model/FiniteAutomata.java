package falt.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

/**
 * A representation of a finite automata machine used in theoretical computation.
 * FiniteAutomata handle non-determinism by storing their "current" state as a set 
 * of their individual states. FiniteAutomata have basic add and remove methods for 
 * states, String acceptance testing functionality, and have a deep copy method.
 * 
 * @author ncameron
 * @author kmcdonell
 */
public class FiniteAutomata implements Serializable {
	
	
	private static final long serialVersionUID = 1392525413424496066L;
	/** Holds the start state of the finite automata. */
	private State start;
	/** Holds a set of states of the finite automata. */
    private final Set<State> states = new HashSet<>();
	
	/** Initializes a new FiniteAutomata instance. */
	public FiniteAutomata(){ }
	
	
	/* ---- Methods pertaining to states ---- */
	
	/**
	 * Returns whether or not the finite automata contains the givenstate 
	 * @param state		the state to be checked
	 * @return			true if the FiniteAutomata contains the state
	 */
	public boolean containsState(State state){
		return states.contains(state);
	}
	
	/**
	 * Adds a newly initialized state to the finite automata.
	 */
	public void addState() { states.add(new State()); }
	
	/**
	 * Adds a given state to the FiniteAutomata.
	 * @param newState		the state to be added, must not be null
	 */
	public void addState(State toAdd) {
		Utility.nonNullArguments(toAdd);
		states.add(toAdd);
	}

	/**
	 * Removes a state from the FiniteAutomata.
	 * @param toRemove		the state to remove, must not be null
	 * @return 				if toRemove was a state in the FiniteAutomata
	 */
	public boolean removeState(State toRemove) {
		Utility.nonNullArguments(toRemove);
		boolean success = false;
		for (Iterator<State> i = states.iterator(); i.hasNext();) {
			State s = i.next();
			if (s == toRemove) {
				i.remove();
				success = true;
			} else {
				s.removeTransition(toRemove);
			}
		}
		return success;
	}
	
	/**
	 * Returns a set of all the states in the finite automata.
	 * 
	 * @return		a Set of the States in the FiniteAutomata
	 */
	public Set<State> getStates() { return states; }
	
	/**
	 * Returns a set of all the accept states in the finite automata.
	 * 
	 * @return		a Set of the accept States in the FiniteAutomata
	 */
	public Set<State> getAcceptStates() {
		Set<State> acceptStates = new HashSet<State>();
		for (State s : states){
			if (s.isAccept())
				acceptStates.add(s);
		}
		return acceptStates;
	}
	
	/**
	 * Changes the start state of the finite automata to the given state.
	 * 
	 * @param start		the new starting State, must be one of the states of the 
	 * 					FiniteAutomata, or will throw an IllegalArgumentException.
	 */
	public void setStart(State start) {
		if(start != null && !states.contains(start))
			throw new IllegalArgumentException("The start state must be one of the "
					+ "states of the FiniteAutomata instance");
		this.start = start;
	}
	
	/**
	 * Returns the start state of the FiniteAutomata.
	 * @return		the start State
	 */
	public State getStart() { return start; }
	
	
	/**
	 * Adds a new transition between two states for a specified Character
	 * to the FiniteAutomata.
	 * 
	 * @param source	the new transition begins at this state of the finite automata, 
	 * 					must be a part of the finite automata and not null or will 
	 * 					throw an IllegalArgumentException
	 * @param target	the new transition begins at this state of the finite automata, 
	 * 					must be a part of the finite automata and not null or will 
	 * 					throw an IllegalArgumentException
	 * @param c			the new transition requires this Character to be traversed
	 * @return 			true if the addition was successful, else false
	 */
	public boolean addTransitions(State source, State target, Character c) {
		Utility.nonNullArguments(source, target);
		enforceContains(source,target);
		return source.addTransitionCharacter(target, c);
	}
	
	/**
	 * Adds a new transition to the FiniteAutomata.
	 * 
	 * @param t			the new transition to be added, must be a transition from
	 * 					two states in this finite automata or will throw an 
	 * 					IllegalArgumentException
	 * @return 			the transition that previously existed between the two 
	 * 					states, or null if there was none
	 */
	public Transition addTransition(Transition t){
		enforceContains(t.getSource(),t.getTarget());
		return t.getSource().addTransition(t.getTarget(), t);
	}
	
	/**
	 * Removes a given transition from the FiniteAutomata.
	 * @param toRemove		the transition to remove, must not be null
	 * @return 				if toRemove was a transition in the FiniteAutomata
	 */
	public boolean removeTransition(Transition toRemove) {
		Utility.nonNullArguments(toRemove);
		State source = toRemove.getSource();
		State target = toRemove.getTarget();
		if (source.getTransition(target) == toRemove){
			source.removeTransition(target);
			return true;
		} else {
			return false;
		}
		 
	}
	
	/**
	 * Gets the transition between two states in the FiniteAutomata.  If no such transition 
	 * exists, null is returned
	 * 
	 * @param source	the state the transition begins at, 
	 * 					must be a part of the finite automata and not null or will 
	 * 					throw an IllegalArgumentException
	 * @param target	the state the transition ends at, 
	 * 					must be a part of the finite automata and not null or will 
	 * 					throw an IllegalArgumentException
	 */
	public Transition getTransition(State source, State target){
		Utility.nonNullArguments(source,target);
		enforceContains(source,target);
		return source.getTransition(target);
	}
	
	/**
	 * Returns a set of the transitions in the FiniteAutomata
	 * @return		a Set of the transitions in the FiniteAutomata
	 */
	public Set<Transition> getTransitions(){
		Set<Transition> transitions = new HashSet<>();
		for (State s : states){
			transitions.addAll(s.getTransitions());
		}
		return transitions;
		
	}
	
	/**
	 * Returns a collection of transitions to a given state.
	 * 
	 * @param state		the State to get the transitions to,
	 * 					must be apart of the finite automata or
	 * 					a will throw an IllegalArgumentException
	 * @return			a Collection of transitions to the State
	 */
	public Collection<Transition> getTransitionsTo(State state) {
		enforceContains(state);
		ArrayList<Transition> results = new ArrayList<>();
		Transition t;
		for(State s : states){
			t = s.getTransition(state);
			if (t != null){
				results.add(t);
			}
		}
		return results;
	}
	
	
	/**
	 * Gets the alphabet of the finite automata and returns it as a Set.
	 * 
	 * @return	a Set containing the Characters in the alphabet
	 */
	public Set<Character> getAlphabet(){
		Set<Character> alphabet = new HashSet<Character>();
		for(State st: states){
			for(Transition t: st.getTransitions()){
				alphabet.addAll(t.getCharacters());
			}
		}
		return alphabet;
	}
	
	
	/**
	 * Simulates a step in the simulation of the automata on the given character
	 * and set of states that the finite automata is in, and returns the set of 
	 * states that the finite automata is in after the transition. Handles 
	 * nondeterminism and empty string transitions.
	 * 
	 * @param c				the character to transition on
	 * @param prevStates	the set of states the finite automata is in before this step
	 */
	public Set<State> step(Character c, Set<State> prevStates) {
		
		Set<State> currentStates = new HashSet<>();
		Queue<State> toFollowEpsilon = new LinkedList<>();

		// Transition using character c on all states in currentState
		for (State currentState: prevStates){
			// If we have not already added a state we transition to,
			// add it to the currentStates and the list of states to 
			// transition with epsilon
			for (State s : currentState.followTransition(c)){
				if (!currentStates.contains(s)){
					currentStates.add(s);
					toFollowEpsilon.add(s);
				}
			}
		}

		// Transition using epsilon transitions
		State curr;
		while (!toFollowEpsilon.isEmpty()) {
			curr = toFollowEpsilon.remove();

			// If we have not already added a state, add it to the list of
			// current states, and add it to the list to follow using epsilon
			for (State s : curr.followTransition(null)) {
				if (!currentStates.contains(s)){
					currentStates.add(s);
					toFollowEpsilon.add(s);
				}
			}
		}
		
		return currentStates;
		
	}
	
	
	/**
	 * Steps through a finite automata on a given string input and returns the set of 
	 * states that the FA is in after running the input.
	 * 
	 * @param input		the input string to run in the FiniteAutomata
	 * @return			the set of states that the FiniteAutomata is in after running
	 * 					the given input
	 * @throws NoStartStateException	if there is no start state in the FiniteAutomata
	 */
	public Set<State> runWithInput(String input) throws NoStartStateException {
		if (start == null)
			throw new NoStartStateException();
		
		Set<State> currentStates = new HashSet<>();
		currentStates.add(start);
		currentStates.addAll(step(null, currentStates));
		
		for (int i = 0; i < input.length(); i++) {
			currentStates = step(input.charAt(i), currentStates);
		}
		
		return currentStates;
	}
	
	
	/**
	 * Tests a set of states to see if it contains an accept state.
	 * 
	 * @param states	the set of states to test for an accept state
	 * @return			true if states contains an accept state, else false
	 */
	public boolean containsAcceptState(Set<State> states) {
		for(State s: states){
			if(s.isAccept())
				return true;
		}
		return false;
	}
	
	
	/**
	 * Tests a given input string on the finite automata and returns whether 
	 * the input is accepted or rejected.
	 * 
	 * @param input		the input string to test in the FiniteAutomata
	 * @return			true if input is accepted, false if input is rejected
	 * @throws NoStartStateException	if there is no start state in the FiniteAutomata
	 */
	public boolean accepts(String input) throws NoStartStateException {
		Set<State> statesReached = runWithInput(input);
		return containsAcceptState(statesReached);
	}
	
	
	/**
	 * Creates a deep copy of this finite automata, and a mapping between old states
	 * and new. 
	 * 
	 * @return		an AutomataCorrespondence between this FiniteAutomata and the
	 * 				copy
	 */
	public AutomataCorrespondence copyWithCorrespondence() {
		HashMap<State, State> pairs = new HashMap<>();
		
		// create the new states
		for (State st: this.states){
			pairs.put(st, st.shellCopy());
		}
		// create the new transitions
		for (State st: this.getStates()){
			for (Transition tr: st.getTransitions()){
				pairs.get(st).addTransitionSet(pairs.get(tr.getTarget()), tr.getCharacters());
			}
		}
		// make the actual copy now that we have all the ingredients
		FiniteAutomata copy = new FiniteAutomata();
		for (State st: pairs.values()){
			copy.addState(st);
		}
		
		copy.setStart(pairs.get(this.getStart()));
		return new AutomataCorrespondence(this,pairs,new ArrayList<State>(),copy);
	}
	
	/**
	 * Creates a deep copy of this finite automata.
	 * @return		a deep copy of the FiniteAutomata
	 */
	public FiniteAutomata copy() {
		return copyWithCorrespondence().target;
	}
	
	
	
	/**
	 * Throws an illegal argument exception if some
	 * of the states in the provided list are not contained
	 * in this automata
	 * 
	 * @param states	a list of states to check
	 */
	private void enforceContains(State... states){
		for (State s : states){
			if (!containsState(s))
				throw new IllegalArgumentException("The state are not in"
						+ " the finite automata");
		}
	}
	
	
	
}
