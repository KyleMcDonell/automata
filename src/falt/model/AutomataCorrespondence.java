package falt.model;

import java.util.List;
import java.util.Map;

/**
 * A tuple class which represents the correspondence between finite automata.
 * There may be up to two source finite automata, and one target. States from
 * the source(s) are mapped to states in the target, and any states in the target
 * finite automata that are not mapped to are held in a separate list. The mapping
 * is {source states} -> {target states}.
 * 
 * @author ncameron
 * @author kmcdonell
 */
public class AutomataCorrespondence {
	
	/** The mapping of source states to target states. */
	public final Map<State,State> stateMap;
	/** List of the target states not in the range of the mapping. */
	public final List<State> otherStates;
	/** The finite automata that comprise the correspondence. */
	public final FiniteAutomata source1, source2, target;
	
	/**
	 * Creates a new one to one correspondence.
	 * 
	 * @param source1		the source finite automata
	 * @param stateMap		the mapping of source states to target states
	 * @param newStates		the list of target states not in the range of the mapping
	 * @param target		the target finite automata
	 */
	public AutomataCorrespondence(FiniteAutomata source1, Map<State,State> stateMap, 
			List<State> newStates, FiniteAutomata target){
		
		this(source1,null,stateMap,newStates,target);
	}
	
	/**
	 * Creates a new correspondence with two source finite automata.
	 * 
	 * @param source1		the first source finite automata
	 * @param source2		the second source finite automata
	 * @param stateMap		the mapping of source states to target states
	 * @param newStates		the list of target states not in the range of the mapping
	 * @param target		the target finite automata
	 */
	public AutomataCorrespondence(FiniteAutomata source1, FiniteAutomata source2, 
			Map<State,State> stateMap, List<State> newStates, FiniteAutomata target){
		
		this.stateMap = stateMap;
		this.otherStates = newStates;
		this.source1 = source1;
		this.source2 = source2;
		this.target = target;
	}
}

