package falt.model;

/**
 * Contains static sample finite automata constructors, mainly for testing.
 * 
 * @author ncameron
 * @author kmcdonell
 */
public class SampleFiniteAutomata {
	
	
	// Each automata recognizes the language in the method name
	
	/**
	 * Creates a new finite automata that accepts epsilon.
	 * @return	a FiniteAutomata that only accepts epsilon
	 */
	public static FiniteAutomata epsilon(){
		FiniteAutomata sample = new FiniteAutomata();
		State s = new State();
		sample.addState(s);
		sample.setStart(s);
		s.setAccept(true);
		return sample;
	}

	/**
	 * Creates a new finite automata that accepts a.
	 * @return	a FiniteAutomata that only accepts a
	 */
	public static FiniteAutomata a(){
		FiniteAutomata sample = new FiniteAutomata();
		State[] states = new State[2];
		for(int i=0;i<states.length;i++)
			states[i] = new State();
		states[0].addTransitionCharacter(states[1], 'a');
		for(State s: states)
			sample.addState(s);
		sample.setStart(states[0]);
		states[1].setAccept(true);
		return sample;
	}

	/**
	 * Creates a new finite automata that accepts b.
	 * @return	a FiniteAutomata that only accepts b
	 */
	public static FiniteAutomata b(){
		FiniteAutomata sample = new FiniteAutomata();
		State[] states = new State[2];
		for(int i=0;i<states.length;i++)
			states[i] = new State();
		states[0].addTransitionCharacter(states[1], 'b');
		for(State s: states)
			sample.addState(s);
		sample.setStart(states[0]);
		states[1].setAccept(true);
		return sample;
	}

	/**
	 * Creates a new finite automata that accepts aa.
	 * @return	a FiniteAutomata that only accepts aa
	 */
	public static FiniteAutomata aa(){
		FiniteAutomata sample = new FiniteAutomata();
		State[] states = new State[3];
		for(int i=0;i<states.length;i++)
			states[i] = new State();
		states[0].addTransitionCharacter(states[1], 'a');
		states[1].addTransitionCharacter(states[2], 'a');
		for(State s: states)
			sample.addState(s);
		sample.setStart(states[0]);
		states[2].setAccept(true);
		return sample;
	}

	/**
	 * Creates a new finite automata that accepts ab.
	 * @return	a FiniteAutomata that only accepts ab
	 */
	public static FiniteAutomata ab(){
		FiniteAutomata sample = new FiniteAutomata();
		State[] states = new State[3];
		for(int i=0;i<states.length;i++)
			states[i] = new State();
		states[0].addTransitionCharacter(states[1], 'a');
		states[1].addTransitionCharacter(states[2], 'b');
		for(State s: states)
			sample.addState(s);
		sample.setStart(states[0]);
		states[2].setAccept(true);
		return sample;
	}

	/**
	 * Creates a new finite automata that accepts bb.
	 * @return	a FiniteAutomata that only accepts bb
	 */
	public static FiniteAutomata bb(){
		FiniteAutomata sample = new FiniteAutomata();
		State[] states = new State[3];
		for(int i=0;i<states.length;i++)
			states[i] = new State();
		
		states[0].addTransitionCharacter(states[1], 'b');
		states[1].addTransitionCharacter(states[2], 'b');
		for(State s: states)
			sample.addState(s);
		sample.setStart(states[0]);
		states[2].setAccept(true);
		return sample;
	}

	/**
	 * Creates a new finite automata that accepts aaaaaaaaaa.
	 * @return	a FiniteAutomata that only accepts aaaaaaaaaa
	 */
	public static FiniteAutomata a10(){
		FiniteAutomata sample = new FiniteAutomata();
		State[] states = new State[11];
		for(int i=0;i<states.length;i++)
			states[i] = new State();
		for(int i=0;i<states.length-1;i++)
			states[i].addTransitionCharacter(states[i+1], 'a');
		for(State s: states)
			sample.addState(s);
		sample.setStart(states[0]);
		states[10].setAccept(true);
		return sample;
	}

	/**
	 * Creates a new finite automata that accepts bbbbbbbbbb.
	 * @return	a FiniteAutomata that only accepts bbbbbbbbbb
	 */
	public static FiniteAutomata b10(){
		FiniteAutomata sample = new FiniteAutomata();
		State[] states = new State[11];
		for(int i=0;i<states.length;i++)
			states[i] = new State();
		for(int i=0;i<states.length-1;i++)
			states[i].addTransitionCharacter(states[i+1], 'b');
		for(State s: states)
			sample.addState(s);
		sample.setStart(states[0]);
		states[10].setAccept(true);
		return sample;
	}

	/**
	 * Creates a new finite automata that accepts a*.
	 * @return	a FiniteAutomata that only accepts a*
	 */
	public static FiniteAutomata aStar(){
		FiniteAutomata sample = new FiniteAutomata();
		State s = new State();
		s.addTransitionCharacter(s, 'a');
		sample.addState(s);
		sample.setStart(s);
		s.setAccept(true);
		return sample;
	}

	/**
	 * Creates a new finite automata that accepts b*.
	 * @return	a FiniteAutomata that only accepts b*
	 */
	public static FiniteAutomata bStar(){
		FiniteAutomata sample = new FiniteAutomata();
		State s = new State();
		s.addTransitionCharacter(s, 'b');
		sample.addState(s);
		sample.setStart(s);
		s.setAccept(true);
		return sample;
	}
	
	
	// Test method
	
	public static void main(String[] args){

		FiniteAutomata epsilon = epsilon();
		FiniteAutomata a = a();
		FiniteAutomata b = b();
		FiniteAutomata aa = aa();
		FiniteAutomata bb = bb();
		FiniteAutomata ab = ab();
		FiniteAutomata a10 = a10();
		FiniteAutomata aStar = aStar();
		FiniteAutomata bStar = bStar();

		try {
			
			System.out.println(epsilon.accepts(""));	//true
			System.out.println(epsilon.accepts("a"));	//false
			System.out.println();

			System.out.println(a.accepts("a"));		//true
			System.out.println(a.accepts(""));		//false
			System.out.println(a.accepts("aa"));	//false
			System.out.println(a.accepts("b"));		//false
			System.out.println(a.accepts("ab"));	//false
			System.out.println();
			
			System.out.println(aa.accepts("aa"));	//true
			System.out.println(aa.accepts("ba"));	//false
			System.out.println(aa.accepts("ab"));	//false
			System.out.println(aa.accepts("aaa"));	//false
			System.out.println(aa.accepts("a"));	//false
			System.out.println(aa.accepts(""));		//false
			System.out.println();

			System.out.println(a10.accepts("aaaaaaaaaa"));	//true
			System.out.println(a10.accepts("a"));			//false
			System.out.println(a10.accepts(""));			//false
			System.out.println();

			System.out.println(aStar.accepts(""));	//true
			System.out.println(aStar.accepts("a"));	//true
			System.out.println(aStar.accepts("aa"));//true
			System.out.println(aStar.accepts("aaa"));//true
			System.out.println(aStar.accepts("b"));	//false
			System.out.println(aStar.accepts("ab"));//false
			System.out.println();
		
		} catch (NoStartStateException e) {
			e.printStackTrace();
		}
	}
}
