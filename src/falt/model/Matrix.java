package falt.model;

/**
 * Static methods for determining properties of matrices.
 */
public class Matrix {

	/**
	 * Returns whether the given 2D double array is a matrix.
	 * 
	 * @param mat	the 2D double array to test
	 * @return		true if mat is a matrix, else false
	 */
	public static boolean isMatrix(double[][] mat){
		// Checks if the matrix is non-empty
		if (mat.length < 1 || mat[0].length < 1)
			return false;
		// Checks if each row has the same length
		int columns = mat[0].length;
		for (double[] array : mat){
			if (array.length != columns)
				return false;
		}
		return true;
	}
	
	/**
	 * Returns whether the given 2D double array is a square matrix.
	 * 
	 * @param mat	the 2D double array to test
	 * @return		true if mat is a matrix, else false
	 */
	public static boolean isSquareMatrix(double[][] mat){
		// Checks if the matrix is non-empty
		if (mat.length < 1 || mat[0].length < 1)
			return false;
		// Checks if each row has the same length as the columns
		for (double[] array : mat){
			if (array.length != mat.length)
				return false;
		}
		return true;
	}
	
	
	/**
	 * Returns the determinant of a given square matrix.
	 * Throws an IllegalArgumentException if the input is not a square matrix.
	 * 
	 * @param mat	the matrix to determine
	 * @return		the double determinant of mat
	 */
	public static double determinant(double[][] mat){
		
		if (!isSquareMatrix(mat))
			throw new IllegalArgumentException();		
		
		// Use the determinant formula for n=1 or n=2
		if (mat.length == 1)
			return mat[0][0];
		else if (mat.length == 2)
			return mat[0][0]*mat[1][1] - mat[1][0]*mat[0][1];
		
		// For n>2, use Laplace cofactor expansion along the first column
		else {
			double det = 0;
			for (int i=0; i<mat.length; i++){
				det += (mat[i][0] * cofactor(mat,i,0));
			}
			return det;
		}
	}
	
	/**
	 * Returns whether a given square matrix is invertible.  
	 * Throws an IllegalArgumentException if the input is not a square matrix.
	 * 
	 * @param mat		the matrix to test for invertibility
	 * @return			true if mat is invertible, else false
	 */
	public boolean isInvertible(double[][] mat){
		return determinant(mat)!=0;
	}
	
	/**
	 * Returns the value of the (row, column) minor of the given square matrix. 
	 * Throws an IllegalArgumentException if the input is not a square matrix.
	 * 
	 * @param mat		the matrix to find the minor of
	 * @param row		the row of the minor
	 * @param col		the column of the minor
	 * @return			the double minor of the matrix
	 */
	public static double minor(double[][] mat, int row, int col){
		if (!isSquareMatrix(mat))
			throw new IllegalArgumentException();
		return determinant(subMatrix(mat,row,col));
	}
	
	/**
	 * Returns the value of the (row, column) cofactor of the given square matrix.
	 * Throws an IllegalArgumentException if the input is not a square matrix.
	 * 
	 * @param mat		the matrix to find the cofactor of
	 * @param row		the row of the cofactor
	 * @param col		the column of the cofactor
	 * @return			the double cofactor of the matrix
	 */
	public static double cofactor(double[][] mat, int row, int col){
		int parity = ((row+col) % 2 == 0) ? 1 : -1;
		double cofactor = parity * minor(mat,row,col);
		return cofactor;
	}
	
	/**
	 * Returns a submatrix of the given matrix with the specified row and column
	 * removed. Throws an IllegalArgumentException if the input is not a matrix.
	 * 
	 * @param mat		the matrix to get the submatrix of
	 * @param row		the row to remove
	 * @param col		the column to remove
	 * @return			the submatrix with row and column removed
	 */
	public static double[][] subMatrix(double[][] mat, int row, int col){
		
		if (!isMatrix(mat))
			throw new IllegalArgumentException();
		
		double[][] subMatrix = new double[mat.length-1][];
		for (int i=0; i<subMatrix.length; i++){
			subMatrix[i] = new double[mat[0].length-1];
		}
		int rowIndex;
		
		// Go through the row index
		for (int i=0; i<mat.length; i++){
			
			// Skip the provided row
			if (i < row){
				rowIndex = i;
			} else if (i > row){
				rowIndex = i-1;
			} else {
				continue;
			}
			
			// Fill the subMatrix, skipping the provided column
			for (int j=0; j<mat[0].length; j++){
				
				if (j < col){
					subMatrix[rowIndex][j] = mat[i][j];
				} else if (j > col){
					subMatrix[rowIndex][j-1] = mat[i][j];
				} else {
					continue;
				}
				
			}
			
		}
		
		return subMatrix;
	}
	
	
	
	
	// test method
	public static void main(String[] args){
		double[][] mat1 = {{1,2,3,4,5,6},{7,8,9,10,11,12},{13,14,15,16,17,18},
				{19,20,21,22,23,24},{25,26,27,28,29,30},{31,32,33,34,35,36}};
		
		
		
		System.out.println(Matrix.determinant(mat1) + " = 0");
		double[][] mat2 = {{1,2,4},{8,15,25},{13,18,1}};
		
		double[][] subMat2 = Matrix.subMatrix(mat2, 2, 0);
		for (double[] array : mat2){
			for (double d : array){
				System.out.print(d + " ");
			}
			System.out.println();
		}
		for (double[] array : subMat2){
			for (double d : array){
				System.out.print(d + " ");
			}
			System.out.println();
		}
		
		System.out.println(Matrix.determinant(mat2) + " = -5");
		double[][] mat3 = {{9,2,7},{11,0,25},{13,18,1}};
		System.out.println(Matrix.determinant(mat3) + " = -2036");
	}
}


