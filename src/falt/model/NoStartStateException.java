package falt.model;

/**
 * An exception to be thrown when a String is passed to a FiniteAutomata for 
 * testing and the FiniteAutomata does not have a start state.
 *  
 * @author ncameron
 * @author kmcdonell
 */
public class NoStartStateException extends Exception {

	private static final long serialVersionUID = 509818464367629202L;


}
