package falt;

import javafx.application.Application;
import javafx.stage.Stage;

import java.io.IOException;

import falt.controller.MenuController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;


/**
 * Main Application for running the program.
 * 
 * @author ncameron
 * @author kmcdonell
 */

public class MainApp extends Application {

	/**
	 * Start the application.  Called automatically
	 */
	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("Finite Automata");
		initRootLayout(primaryStage);
	}
	
	/**
	 * Initializes the root layout.
	 */
	private void initRootLayout(Stage primaryStage){
		 try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/RootLayout.fxml"));
            BorderPane rootLayout = (BorderPane) loader.load();
            MenuController controller = (MenuController)loader.getController();
            controller.setStage(primaryStage);
            controller.newAutomataTab(null);
            
            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
            
        } catch (IOException e) {
            e.printStackTrace();
        }
	}
	
	/**
	 * Launches the application.
	 * @param args	command line args, no effect
	 */
	public static void main(String[] args) {
		launch(args);
	}
}
